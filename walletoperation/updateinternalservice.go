package walletoperation

import (
	"context"
	"fmt"
	"grpcwallet/walletservice"
	"time"

	"github.com/jackc/pgx/v4"
)

func UpdateInternalService(ctx context.Context, tx pgx.Tx, debitreq walletservice.Walletperform, creditreq walletservice.Walletperform) error {
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "Wallet ID:", debitreq.WalletId, "Api Wallet ID:", debitreq.ApiWalletId)
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "of amount:", debitreq.Amount)
	fmt.Println("Txn id in request for update credit: ", creditreq.Id, "Wallet ID:", creditreq.WalletId, "Api Wallet ID:", creditreq.ApiWalletId)
	fmt.Println("Txn id in request for update credit: ", creditreq.Id, "of amount:", creditreq.Amount)
	if debitreq.WalletId != 0 && creditreq.WalletId != 0 {

		//Debit
		 if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet WHERE id = $1 FOR UPDATE", debitreq.WalletId).Scan(&debitreq.PreviousBalance, &debitreq.MinimumBalance); err != nil {

			fmt.Println(err)
			return err

		}
		// fmt.Println("minbal ", debitreq.MinimumBalance)
		// if debitreq.PreviousBalance < debitreq.Amount {
		// 	return errors.New("7")
		// }
		// } else if debitreq.PreviousBalance-debitreq.Amount < debitreq.MinimumBalance {
		// 	return errors.New("5")
		// }
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.WalletId); err != nil {
			return err
		}

		debitreq.CurrentBalance = debitreq.PreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate=$5 where id = $1", debitreq.Id, debitreq.PreviousBalance, debitreq.CurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		fmt.Println("Debited Successfully")
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		//Credit
		//var apiPreviousBalance1 float64
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet WHERE id = $1 FOR UPDATE", creditreq.WalletId).Scan(&creditreq.PreviousBalance, &creditreq.MaximumBalance); err != nil {
			//fmt.Println(err)
			return err
		}
		// if creditreq.PreviousBalance+creditreq.Amount >= creditreq.MaximumBalance {
		// 	return errors.New("10")
		// }
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.WalletId); err != nil {
			return err
		}
		creditreq.CurrentBalance = creditreq.PreviousBalance + creditreq.Amount

		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", creditreq.Id, creditreq.PreviousBalance, creditreq.CurrentBalance, "SUCCESS",
			creditreq.UpdatedDate); err != nil {
			return err
		}
		fmt.Println("Credited Successfully")
		creditreq.Status = "SUCCESS"
		go walletservice.PubsubData(creditreq)
		return nil

	} else if debitreq.ApiWalletId != 0 && creditreq.ApiWalletId != 0 {

		//Debit
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", debitreq.ApiWalletId).Scan(&debitreq.ApiPreviousBalance, &debitreq.ApiminimumBalance); err != nil {

			fmt.Println(err)
			return err

		}
		//fmt.Println("minbal ", debitreq.ApiminimumBalance)
		// if debitreq.ApiPreviousBalance < debitreq.Amount {
		// 	return errors.New("8")
		// }
		// } else if debitreq.ApiPreviousBalance-debitreq.Amount < debitreq.ApiminimumBalance {
		// 	return errors.New("6")
		// }
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.ApiWalletId); err != nil {
			return err
		}

		debitreq.ApiCurrentBalance = debitreq.ApiPreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate=$5 where id = $1", debitreq.ApiId, debitreq.ApiPreviousBalance, debitreq.ApiCurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		fmt.Println("Debited Successfully in api")
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)

		//Credit
		//var apiPreviousBalance1 float64
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", creditreq.ApiWalletId).Scan(&creditreq.ApiPreviousBalance, &creditreq.ApimaximumBalnce); err != nil {
			//fmt.Println(err)
			return err
		}
		// if creditreq.ApiPreviousBalance+creditreq.Amount >= creditreq.ApimaximumBalnce {
		// 	return errors.New("9")
		// }
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.ApiWalletId); err != nil {
			return err
		}

		creditreq.ApiCurrentBalance = creditreq.ApiPreviousBalance + creditreq.Amount

		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", creditreq.ApiId, creditreq.ApiPreviousBalance, creditreq.ApiCurrentBalance, "SUCCESS",
			creditreq.UpdatedDate); err != nil {
			return err
		}
		fmt.Println("Credited Successfully in api")
		creditreq.Status = "SUCCESS"
		go walletservice.PubsubData(creditreq)
	}
	return nil
}
