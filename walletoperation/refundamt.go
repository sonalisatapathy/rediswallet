package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
	"math"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

func (s *Server) RefundAmt(stream protobufpb.WalletService_RefundAmtServer) error {
	fmt.Println("Inside Refund Amount")
	for {
		r, err := stream.Recv()
		if r == nil {
			fmt.Println("Inside Refund::Request data is empty:", r)
			break
		} else if err != nil {
			fmt.Println("Inside Refund Amount::Error in receive stream", err)
			continue
		}
		fmt.Println("Txn id in request for refund: ", r.Txnid, "Gateway Transaction ID:", r.GatewayTxnID, "Amount:", r.Amount)
		fmt.Println("Txn id in request for refund: ", r.Txnid, "Wallet Ledger ID:", r.Walletledgerid, "ApiWallet Ledger ID:", r.Apiwalletledgerid)
		fmt.Println("Txn id in request for refund: ", r.Txnid, "Status Code:", r.StatusCode, "IS Partial Refund:", r.Ispartialrefund, "Transaction Type:", r.TransactionType)
		res := &protobufpb.Refundresponse{}
		refundreq := walletservice.Walletperform{}
		response, checkerr := walletservice.Checkvalidationforrefundamount(r)
		if checkerr != nil {
			fmt.Println("Txn id in request for refund: ", r.Txnid, "Check error", checkerr)
			sendErr := stream.Send(response)
			if sendErr != nil {
				fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
			}
			continue
		}
		conn, err := s.Conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Refundwallet:Exception in conn acquire:insert wallet", err.Error())
			continue
		}
		var debitAmount, walletDebitAmount float64
		var createddate time.Time
		selectapierr := conn.QueryRow(context.Background(), "SELECT id,amount,walletid,createddate FROM wallet_api_ledger WHERE txnid=$1 and type=1 and status= 'SUCCESS'", r.GetTxnid()).Scan(&refundreq.ApiReversalId, &debitAmount, &refundreq.ApiWalletId, &createddate)
		if selectapierr != nil {
			fmt.Println("Txn id in request for refund: ", r.Txnid, "Not found", selectapierr)
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "transaction not available with the txn id"
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
		} else {
			refundreq.Is_sl = true
		}
		selecterr := conn.QueryRow(context.Background(),
			"SELECT id,amount,walletid,createddate FROM wallet_ledger WHERE txnid=$1 and type=1 and status= 'SUCCESS'", r.Txnid).Scan(&refundreq.ReversalId, &walletDebitAmount, &refundreq.WalletId, &createddate)
		if selecterr != nil {
			fmt.Println("Txn id in request for refund: ", r.Txnid, "Not found", selecterr)
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "transaction not available with the txn id"
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
		} else {
			refundreq.Is_sl = false
		}
		conn.Release()
		if selectapierr != nil && selecterr != nil {
			stream.Send(res)
			return nil
		}
		if createddate.AddDate(0, 0, 45*1).Before(time.Now()) && selectapierr == nil && selecterr == nil {
			fmt.Println("Txn id in request for not refund: ", r.Txnid, "after 45 days,Created date", createddate)
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "transaction not refunded after 45 days"
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
			sendErr := stream.Send(res)
			if sendErr != nil {
				fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
			}
			continue
		}
		if (r.Amount > debitAmount || r.Amount > walletDebitAmount) && (debitAmount != 0 || walletDebitAmount != 0) {
			fmt.Println("Txn id in request for not refund: ", r.Txnid, "Request Amount is greater than debited amount", r.Amount)
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "Request Amount is greater than debited amount"
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
			sendErr := stream.Send(res)
			if sendErr != nil {
				fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
			}
			continue
		}
		if !r.Ispartialrefund {
			r.Amount = debitAmount
		}

		checkconn, err := s.Conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Refundwallet:Exception in conn acquire:insert wallet", err.Error())
			continue
		}
		var sumamount float64
		checkdebiterr := checkconn.QueryRow(context.Background(), "SELECT COALESCE(sum(amount),0) from wallet_ledger where reversalid = $1", r.GetTxnid()).Scan(&sumamount)
		fmt.Println("Total credit", sumamount)
		fmt.Println("Request amount", r.Amount)
		fmt.Println("Debit amount", debitAmount)
		var wallet_refund = sumamount + r.Amount
		if checkdebiterr != nil {
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "Error in fetching debit amount " + checkdebiterr.Error()
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
			sendErr := stream.Send(res)
			if sendErr != nil {
				fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", checkdebiterr)
			}
			continue
		}
		if math.Floor(wallet_refund*100)/100 > debitAmount {
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "Can't refund limit exceed of amount" //+ fmt.Sprint(sumamount-r.Amount)
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
		}
		var apisumamount float64
		checkapierr := checkconn.QueryRow(context.Background(), "SELECT COALESCE(sum(amount),0) from wallet_api_ledger where reversalid = $1", r.GetTxnid()).Scan(&apisumamount)

		fmt.Println("Total api credit", apisumamount)
		fmt.Println("Request amount", r.Amount)
		fmt.Println("Debit api amount", walletDebitAmount)

		var wallet_api_refund = apisumamount + r.Amount
		if checkapierr != nil {
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "Error in fetching debit amount " + checkdebiterr.Error()
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
			sendErr := stream.Send(res)
			if sendErr != nil {
				fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", checkdebiterr)
			}
			continue
		}
		if math.Floor(wallet_api_refund*100)/100 > walletDebitAmount {
			res.Txnid = r.Txnid
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = "Can't refund limit exceed of  amount" //+ fmt.Sprint(apisumamount-r.Amount)
			res.Walletstatuscode = -1
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
		}
		checkconn.Release()
		if res.Walletstatus == "FAILED" {
			stream.Send(res)
			return nil
		}

		refundreq.Amount = float64(r.GetAmount())
		refundreq.TxnId = r.GetGatewayTxnID()
		refundreq.ReversalId = r.GetTxnid()
		refundreq.ApiReversalId = r.GetTxnid()
		refundreq.Type = 0 //For Credit
		refundreq.TransactionType = r.GetTransactionType()
		refundreq.RefundStatusCode = r.GetRefundStatusCode()
		refundreq.Ispartial = r.GetIspartialrefund()
		conn1, err := s.Conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Creditwallet:Excption in conn acquire:insert wallet", err.Error())
		}
		refundreq, err1 := walletservice.InsertWalletledger(context.Background(), conn1, refundreq)
		conn1.Release()
		if err1 == nil {
			conn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside Refundwallet:Exception in conn acquire:update wallet", err.Error())
			}
			err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
				err := updateWalletCredit(context.Background(), tx, refundreq)
				return err
			})
			conn.Release()
			if err == nil {
				res.Txnid = r.Txnid
				res.Walletstatus = "SUCCESS"
				res.Walletstatusdesc = "Successfully credited"
				res.Wallettransactionid = refundreq.Id
				res.Apiwallettransactionid = refundreq.ApiId
				res.Walletstatuscode = 0
				res.GatewayTxnID = r.GatewayTxnID
				res.StatusCode = r.StatusCode
				fmt.Println("Inside Refundwallet::Response Data::Transaction ID::", r.Txnid, ":Status:", res.Walletstatus)
				if !refundreq.Ispartial {
					conn, err := s.Conn.Acquire(context.Background())
					if err != nil {
						fmt.Println("Inside Refundwallet:Exception in conn acquire:update wallet", err.Error())
					}

					err = updatestatus(context.Background(), conn, r, refundreq)
					if err != nil {
						fmt.Println("error in update status code", err.Error())

					}
					conn.Release()
				}
			} else if err != nil {
				fmt.Println("Inside Refundwallet::Error::Transaction ID::", r.Txnid, ":Failure Status desc:", err.Error())
				res.Txnid = r.Txnid
				res.Walletstatus = "FAILED"
				res.Wallettransactionid = refundreq.Id
				res.Apiwallettransactionid = refundreq.ApiId
				res.GatewayTxnID = r.GatewayTxnID
				res.StatusCode = r.StatusCode

				switch err.Error() {
				case "9":
					res.Walletstatusdesc = "Maximum Balnce Limit exceed of API user"
					res.Walletstatuscode = 5
				case "10":
					res.Walletstatusdesc = "Maximum Balance Limit exceed of user"
					res.Walletstatuscode = 5
				default:
					res.Walletstatusdesc = err.Error()
					res.Walletstatuscode = 7
				}

				//refund amount conenction
				conn, connerr := s.Conn.Acquire(context.Background())
				if connerr != nil {
					fmt.Println("Txn id in request for refund::", r.GetTxnid(), "Error in conn acquire in update :", connerr.Error())
				}
				walletservice.UpdateRefund(refundreq, res, conn)
				conn.Release()
				fmt.Println("Inside refundwallet::Response Data::Transaction ID::", r.Txnid, ":Status:", res.Walletstatus)
				fmt.Println("Inside refundwallet::Response Data::Transaction ID::", r.Txnid, ":Failure Status desc:", err.Error())
			}
			//	stream.Send(res)
		} else {
			res.Txnid = r.Txnid
			res.Wallettransactionid = refundreq.Id
			res.Apiwallettransactionid = refundreq.ApiId
			res.GatewayTxnID = r.GatewayTxnID
			res.StatusCode = r.StatusCode
			res.Walletstatus = "FAILED"
			res.Walletstatusdesc = err1.Error()
			res.Walletstatuscode = 2
		}
		sendErr := stream.Send(res)
		if sendErr != nil {
			fmt.Println("Inside Refundwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
		}

	}

	return nil
}
