package walletoperation

import (
	"context"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
)

func (s *Server) CheckBalance(ctx context.Context, r *protobufpb.CheckBalanceRequest) (*protobufpb.CheckBalanceResponse, error) {
	response := &protobufpb.CheckBalanceResponse{}
	conn := s.Conn
	var balance float64
	balance, err := walletservice.Checkwallet(conn, r.Walletid)
	if err == nil {
		response.Balance = balance
		response.Status = "SUCCESS"
		response.Walletid = r.GetWalletid()
		return response, nil
	} else {
		response.Balance = 0
		response.Status = "FAILED"
		response.Walletid = r.GetWalletid()
		return response, nil
	}

}
