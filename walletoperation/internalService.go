package walletoperation

import (
	"context"
	"fmt"
	protobuftest "grpcwallet/src/protobuf/protobuftest"
	"grpcwallet/walletservice"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"

	"github.com/jackc/pgx/v4"
)

func (s *Server) InternalService(stream protobuftest.ServiceWallet_InternalServiceServer) error {
	fmt.Println("Inside InternalService")

	for {
		r, err := stream.Recv()
		if r == nil {
			fmt.Println("Insdie  InternalService, request is", r)
			break
		} else if err != nil {
			fmt.Println("Inside  InternalService::Error in stream receive:", err)
			continue
		}
		fmt.Println("Inside InternalService::Request Data::Transaction ID:", r.Tnxid, "::Transaction Type:", r.TransactionType)
		fmt.Println("Inside InternalService::Request Data::Transaction ID::", r.Tnxid, "Debit Wallet ID:", r.DebitWallet, "Credit Wallet ID:", r.CreditWallet)
		fmt.Println("Inside InternalService::Request Data::Transaction ID::", r.Tnxid, "Amount:", r.Amount, "Single Layer:")

		
		debitreq := walletservice.Walletperform{}
		creditreq := walletservice.Walletperform{}
		
		response, checkerr := walletservice.Checkvalidationforinterwalletservice(r)
		if checkerr != nil {
			sendErr := stream.Send(response)
			if sendErr != nil {
				fmt.Println("Inside InternalService::Stream send error::", r.Tnxid, ":Failure desc:", sendErr)
				continue
			}
		}else {
			if r.GetSeriveType() == 0 {
				//If service type is zero then for customer interwallet
				debitreq.Amount = float64(r.GetAmount())
				debitreq.WalletId = r.GetDebitWallet()
				debitreq.TxnId = r.GetTnxid()
				debitreq.Type = 1
				debitreq.Is_sl = true
				debitreq.TransactionType = r.GetTransactionType()

				creditreq.Amount = float64(r.GetAmount())
				creditreq.WalletId = r.GetCreditWallet()
				creditreq.TxnId = r.GetTnxid()
				creditreq.Type = 0
				creditreq.Is_sl = true
				creditreq.TransactionType = r.GetTransactionType()

				checkconn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				count, err := walletservice.Checkwalletavailable(context.Background(), checkconn, debitreq.WalletId, creditreq.WalletId)
				checkconn.Release()
				fmt.Println(count)
				if count != 2 {
					response.Txnid = r.Tnxid
					response.Walletstatus = "Failed"
					response.Walletstatusdesc = " CreditWallet or DebitWallet is not available"
					response.Debittransactionid = 0
					response.Credittransactionid = 0
					response.Walletstatuscode = 6
					stream.Send(response)
					return err
				}
				//call check min balance func() for user
				conn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				err=walletservice.CheckMinBalanceStatusUser(context.Background(),conn,debitreq)
				conn.Release()
				if err != nil {
					switch err.Error() {
					case "5":
						response.Txnid = r.Tnxid
						response.Walletstatus = "Failed"
						response.Walletstatusdesc = "Minimum Balance not available in userwallet"
						response.Walletstatuscode = 3
					
					case "7":
						response.Txnid = r.Tnxid
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Balance not available in userwallet"
						response.Walletstatuscode = 4
					default:
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = err.Error()
						response.Walletstatuscode = 6
					
					}
					stream.Send(response)
					return err
				}
				//call check for maximum balance for userwallet
				conn, err = s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				err=walletservice.CheckMaxBalanceStatusUser(context.Background(),conn,creditreq)
				conn.Release()
				if err != nil {
					response.Txnid = r.Tnxid
					response.Walletstatus = "Failed"
					response.Walletstatusdesc = "Maximum Balance Limit Exceed in userwallet"
					response.Debittransactionid = 0
					response.Credittransactionid = 0
					response.Walletstatuscode = 5
					stream.Send(response)
					return err
				}
				
			
			} else {
				//FOr internal interwallet
				debitreq.Amount = float64(r.GetAmount())
				debitreq.ApiWalletId = r.GetDebitWallet()
				debitreq.TxnId = r.GetTnxid()
				debitreq.Type = 1
				debitreq.Is_sl = true
				debitreq.TransactionType = r.GetTransactionType()

				creditreq.Amount = float64(r.GetAmount())
				creditreq.ApiWalletId = r.GetCreditWallet()
				creditreq.TxnId = r.GetTnxid()
				creditreq.Type = 0
				creditreq.Is_sl = true
				creditreq.TransactionType = r.GetTransactionType()

				checkconn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				countapi, err := walletservice.Checkwalletapiavailable(context.Background(), checkconn, debitreq.ApiWalletId, creditreq.ApiWalletId)
				checkconn.Release()
				fmt.Println(countapi)
				if countapi != 2 {
					response.Txnid = r.Tnxid
					response.Walletstatus = "Failed"
					response.Walletstatusdesc = " CreditWallet or DebitWallet is not available"
					response.Debittransactionid = 0
					response.Credittransactionid = 0
					response.Walletstatuscode = 6
					stream.Send(response)
					return err
				}
				//call check min balance func() for api_user
				conn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				err=walletservice.CheckMinBalanceStatusApi(context.Background(),conn,debitreq)
				conn.Release()
				if err != nil {
					switch err.Error() {
					case "6":
						response.Txnid = r.Tnxid
						response.Walletstatus = "Failed"
						response.Walletstatusdesc = "Minimum Balance not available in Apiwallet"
						response.Walletstatuscode = 3
					
					case "8":
						response.Txnid = r.Tnxid
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Balance not available in Apiwallet"
						response.Walletstatuscode = 4
					default:
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = err.Error()
						response.Walletstatuscode = 6
					
					}
					stream.Send(response)
					return err
				}
				conn, err = s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				err=walletservice.CheckMaxBalanceStatusApi(context.Background(),conn,creditreq)
				conn.Release()
				if err != nil {
					response.Txnid = r.Tnxid
					response.Walletstatus = "Failed"
					response.Walletstatusdesc = "Maximum Balance Limit Exceed in Apiwallet"
					response.Debittransactionid = 0
					response.Credittransactionid = 0
					response.Walletstatuscode = 5
					stream.Send(response)
					return err
				}
			}

			insertconn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
			}

			debitreq, err1 := walletservice.InsertWalletledger(context.Background(), insertconn, debitreq)

			insertconn.Release()

			insertapiconn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
			}

			creditreq, err2 := walletservice.InsertWalletledger(context.Background(), insertapiconn, creditreq)

			insertapiconn.Release()

			if err1 == nil && err2 == nil {
				comconn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside InternalService:Exception in conn acquire:insert wallet", err.Error())
				}
				err = crdbpgx.ExecuteTx(context.Background(), comconn, pgx.TxOptions{}, func(tx pgx.Tx) error {
					err := UpdateInternalService(context.Background(), tx, debitreq, creditreq)
					//fmt.Println(err.Error())
					return err

				})

				comconn.Release()
				if err == nil {
					response.Txnid = r.Tnxid
					response.Walletstatus = "SUCCESS"
					response.Walletstatusdesc = "Interwallet Operation Successfully Done"
					if r.GetSeriveType() == 0 {
						response.Debittransactionid = debitreq.Id
						response.Credittransactionid = creditreq.Id
					} else {
						response.Debittransactionid = debitreq.ApiId
						response.Credittransactionid = creditreq.ApiId
					}

					response.Walletstatuscode = debitreq.StatusCode
					fmt.Println("Inside InternalService ::Response Data::Transaction ID::", r.Tnxid, ":Status:", response.Walletstatus)
				} else {
					fmt.Println("Txn id in request for debit: ", r.Tnxid, "exception in update :", err)
					response.Txnid = r.Tnxid
					if r.GetSeriveType() == 0 {
						response.Debittransactionid = debitreq.Id
						response.Credittransactionid = creditreq.Id
					} else {
						response.Debittransactionid = debitreq.ApiId
						response.Credittransactionid = creditreq.ApiId
					}
					fmt.Println("Inside InternalService ::Exception in Transaction::Transaction ID::", r.Tnxid, ":Status:", response.Walletstatus)
					//switch err.Error() {
					// case "5":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "Minimum balance not available"
					// 	response.Walletstatuscode = 3
					// case "6":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "API minimum balance not available"
					// 	response.Walletstatuscode = 3
					// case "7":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "Balance not available"
					// 	response.Walletstatuscode = 4
					// case "8":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "API balance not available"
					// 	response.Walletstatuscode = 4
					// case "9":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "Maximum Balnce Limit exceed of API user"
					// 	response.Walletstatuscode = 4
					// case "10":
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = "Maximum Balance Limit exceed of user"
					// 	response.Walletstatuscode = 4
					// default:
					// 	response.Walletstatus = "FAILED"
					// 	response.Walletstatusdesc = err.Error()
					// 	response.Walletstatuscode = 6

					//}
					//Updating status
					conn, err := s.Conn.Acquire(context.Background())
					if err != nil {
						fmt.Println("Txn id in request for debit::", r.GetTnxid(), "Error in conn acquire in update :", err.Error())
					}
					fmt.Println("Debit id", debitreq.Id)
					fmt.Println("Creditreq ", creditreq.Id)
					fmt.Println("Debit id API", debitreq.ApiId)
					fmt.Println("Creditreq API ", creditreq.ApiId)
					fmt.Println()
					walletservice.UpdateInterservicestatus(debitreq, creditreq, response, r, conn)
					conn.Release()
					sendErr := stream.Send(response)
					if sendErr != nil {
						fmt.Println("Inside InternalService::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
						continue
					}

					continue
				}
				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside InternalService::Stream send error::", r.Tnxid, ":Failure desc:", sendErr)
					continue
				}
			} else {
				response.Txnid = r.Tnxid
				if err1 != nil {
					switch err1.Error() {
					case "1":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API Wallet ID is not available"
						response.Walletstatuscode = 2
					case "2":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID is not available"
						response.Walletstatuscode = 2
					case "3":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID is not active"
						response.Walletstatuscode = 1
					case "4":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API wallet ID is not active"
						response.Walletstatuscode = 1
					case "5":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
						response.Walletstatuscode = 1
					default:
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = err1.Error()
						response.Walletstatuscode = 6
					}
				} else if err2 != nil {
					switch err2.Error() {
					case "1":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API Wallet ID is not available"
						response.Walletstatuscode = 2
					case "2":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID is not available"
						response.Walletstatuscode = 2
					case "3":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID is not active"
						response.Walletstatuscode = 1
					case "4":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "API wallet ID is not active"
						response.Walletstatuscode = 1
					case "5":
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
						response.Walletstatuscode = 1
					default:
						response.Walletstatus = "FAILED"
						response.Walletstatusdesc = err2.Error()
						response.Walletstatuscode = 6
					}
				}

				response.Debittransactionid = 0
				response.Credittransactionid = 0
				stream.Send(response)

			}

		}

	}

	return nil
}
