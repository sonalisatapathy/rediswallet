package walletoperation

import (
	"context"
	"encoding/json"
	"fmt"
	"grpcwallet/walletservice"
	"strconv"
	"time"

	"cloud.google.com/go/pubsub"
	"github.com/jackc/pgx/v4/pgxpool"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	//"main.go/operation"
)

type Data struct {
	Key     string `json:"key"`
	Message struct {
		Amount                 float64 `json:"amount"`
		ApiWallet_id           int64   `json:"apiwalletId"`
		Gateway_transaction_id string  `json:"gatewayTransactionId"`
		IsSl                   bool    `json:"isSl"`
		ParentTxn_id           string  `json:"parentTxn_id"`
		Txnid                  string  `json:"txnid"`
		Transaction_type       string  `json:"transactionType"`
		Types                  int     `json:"type"`
		User_id                int64   `json:"userId"`
		Url                    string  `json:"url"`
		Wallet_id              int64   `json:"walletId"`
	} `json:"message"`
}

type Apiresp struct {
	Apiwallettransactionid int64 `json:"apiwallettransactionid"`
	Gateway_transaction_id int64 `json:"gatewayTransactionId"`
	Txnid                  int64 `json:"txnid"`
	ParentTxn_id           int64 `json:"parentTxn_id"`
	//Transaction_type       string `json:"transactionType"`
	User_id             int64  `json:"userId"`
	Walletstatus        string `json:"walletstatus"`
	Walletstatuscode    int32  `json:"walletstatuscode"`
	Wallettransactionid int64  `json:"wallettransactionid"`
	Walletstatusdesc    string `json:"walletstatusdesc"`
}

const projectID = "iserveustaging"
const subID = "retrywalletstaging-sub"

var (
	client *pubsub.Client

	count1 int64
	ctx    = context.Background()
)

// func init() {
// 	// err1 := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "app/iserveustaging-2a27f856e930.json")

// 	// if err1 != nil {

// 	// 	fmt.Println("unable to set", err1)

// 	// }
// 	var err1 error

// 	client, err1 = pubsub.NewClient(ctx, projectID)
// 	if err1 != nil {
// 		//fmt.Errorf("pubsub.NewClient: %v", err1.Error())
// 		fmt.Println("error", err1.Error())
// 	}
// 	//defer client.Close()
// }

func Transactionclear(conn *pgxpool.Pool) error {
	//var commreq Walletperform
	var walletreq walletservice.Walletperform
	fmt.Println("enter for subscription")
	//var resp Apiresp
	var Resp Apiresp
	sub := client.Subscription(subID)
	ctx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()
	// Create a channel to handle messages to as they come in.
	cm := make(chan *pubsub.Message)
	defer close(cm)
	go func() {
		for msg := range cm {
			fmt.Println("Pull Msg Id", msg.ID)
			if msg.ID != " " {
				data := Data{}

				err := json.Unmarshal(msg.Data, &data)
				if err != nil {
					fmt.Println("Error in unmarshal", err.Error())
					continue
				}
				jsonValue, _ := json.Marshal(data)
				count1 += 1
				fmt.Println("Pending data pull counter", count1, "-->for txn_id:", data.Message.Txnid, "-->for wallet id:", data.Message.Wallet_id, "-->for Apiwallet id:", data.Message.ApiWallet_id)
				fmt.Println("Pending datas are", string(jsonValue))
				//req := &protobufpbstatus.StatusqueryRequest{}
				walletreq.TxnId, err = strconv.ParseInt(data.Message.Txnid, 10, 64)
				if err != nil {
					fmt.Println("pull transaction_id conversion error:", err)
					continue

				}
				walletreq.WalletId = data.Message.Wallet_id
				walletreq.ApiWalletId = data.Message.ApiWallet_id
				walletreq.Amount = data.Message.Amount
				// walletreq.Is_sl = true   //Testing purpose
				walletreq.Is_sl = data.Message.IsSl
				walletreq.TransactionType = data.Message.Transaction_type
				walletreq.Type = data.Message.Types
				Resp.Txnid, err = strconv.ParseInt(data.Message.Txnid, 10, 64)
				if err != nil {
					fmt.Println("pull transaction_id conversion error:", err)
					continue
				}
				Resp.Gateway_transaction_id, err = strconv.ParseInt(data.Message.Gateway_transaction_id, 10, 64)
				if err != nil {
					fmt.Println("error in convert gateway txnid", err)
					continue

				}
				Resp.User_id = data.Message.User_id
				Resp.ParentTxn_id, err = strconv.ParseInt(data.Message.ParentTxn_id, 10, 64)
				if err != nil {
					fmt.Println("pull transaction_id conversion error:", err)
					continue
				}
				Url := data.Message.Url //Url coming from request  //URL validation ??
				//	var Reqbody Wallet_req
				Reqbody, err := Status_q(walletreq, conn) //Status query to check status
				fmt.Println("Resp", Reqbody)
				if err != nil {
					fmt.Println("Error in status chaeck ", err, "of txnid ", walletreq.TxnId)
					Resp.Walletstatus = "FAILED"
					Resp.Walletstatuscode = -1         //need to change
					status, err := Sendresp(Resp, Url) //URL validation ??
					if err != nil {
						fmt.Println("error in sending req", err, "of txnid", walletreq.TxnId)
						fmt.Println("status", status)
					} else {
						fmt.Println("successfully response send", status, "of txnid", walletreq.TxnId)
					}

				} else {
					if Reqbody.Status == "INITIATED" {
						fmt.Println("status is initiated")
						walletreq.Id = Reqbody.Id
						walletreq.Status = Reqbody.Status
						walletreq.Type = Reqbody.Type
						walletreq.StatusCode = Reqbody.Statuscode
						walletreq.Amount = Reqbody.Amount
						walletreq.ApiId = Reqbody.ApiId
						Resp, err = Initiated(walletreq, conn, Url, Resp) //Initiated case
						if err != nil {
							Resp.Txnid = walletreq.TxnId
							Resp.Walletstatus = "FAILED"
							Resp.Walletstatusdesc = "ERROR" + err.Error()
							Resp.Wallettransactionid = walletreq.Id
							Resp.Apiwallettransactionid = walletreq.ApiId
							Resp.Walletstatuscode = Resp.Walletstatuscode
							fmt.Println("wallet operation completed for status cheack", "of txnid", walletreq.TxnId)

						}
						status, err := Sendresp(Resp, Url)
						if err != nil {
							fmt.Println("error in sending req", err)
							fmt.Println("status", status)
						} else {
							fmt.Println("successfully response send", status)
							msg.Ack()
						}

					} else if Reqbody.Status == "SUCCESS" || Reqbody.Status == "FAILED" {
						fmt.Println("status is SUCCESS OR FAILED")
						Resp.Walletstatus = Reqbody.Status
						Resp.Walletstatuscode = Reqbody.Statuscode
						Resp.Wallettransactionid = Reqbody.Id
						Resp.Apiwallettransactionid = Reqbody.ApiId
						if Resp.Walletstatuscode == 0 && Reqbody.Type == 0 {
							Resp.Walletstatusdesc = "Successfully credited"
						} else if Resp.Walletstatuscode == 0 && Reqbody.Type == 1 {
							Resp.Walletstatusdesc = "Successfully Debited"
						} else {
							switch Resp.Walletstatuscode {
							case 3:
								Resp.Walletstatusdesc = "Minimum balance not available"
							case 4:
								Resp.Walletstatusdesc = "Balance not available"
							case 5:
								Resp.Walletstatusdesc = "Maximum Balance Limit exceed of user"
							default:
								Resp.Walletstatusdesc = "Duplicate ID or failed to conn database"
							}
						}
						fmt.Println("wallet operation completed for status cheack", "of txnid", walletreq.TxnId)
						status, err := Sendresp(Resp, Url)
						if err != nil {
							fmt.Println("error in sending req", err)
							fmt.Println("status", status)

						} else {
							fmt.Println("successfully response send", status)
							msg.Ack()
						}

						//Resp.Walletstatusdesc = res.WalletStatusDesc //change what to send
					} else {
						fmt.Println("Trsacation id not found")
						fmt.Println("type", walletreq.Type)
						Resp, err = Walletop(walletreq, conn, Url, Resp)
						if err != nil {
							Resp.Txnid = walletreq.TxnId
							Resp.Walletstatus = "FAILED"
							Resp.Walletstatusdesc = "ERROR" + err.Error()
							// Resp.Wallettransactionid = Resp.Wallettransactionid
							// Resp.Apiwallettransactionid = Resp.Apiwallettransactionid
							// Resp.Walletstatuscode = Resp.Walletstatuscode

							fmt.Println("wallet operation completed for status cheack", "of txnid", walletreq.TxnId)

						}
						status, err := Sendresp(Resp, Url)
						if err != nil {
							fmt.Println("error in sending req", err)
							fmt.Println("status", status)

						} else {
							fmt.Println("successfully response send", status)
							msg.Ack()
						}

					}

				}
				//return
				if count1 == 100 {
					return
				}

			} else {
				fmt.Println("pubsub ID not  available in the topic")
			}
			fmt.Println("PULL END")
			//msg.Ack()

		}

	}()

	fmt.Println("PULL START")
	// Receive blocks until the context is cancelled or an error occurs.
	err := sub.Receive(ctx, func(ctx context.Context, msg *pubsub.Message) {

		cm <- msg

	})

	if err != nil && status.Code(err) != codes.Canceled {
		fmt.Println("receive error:", err)
	}
	fmt.Println("PULL COMPLETED")
	return nil
	//count1 = 0
}
