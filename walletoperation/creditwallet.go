package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/vendor/github.com/gomodule/redigo/redis"
	"time"

	"grpcwallet/walletservice"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Server struct {
	Conn *pgxpool.Pool
}
type Server2 struct {
	Pool redis.Pool
}

func (s *Server) CreditWallet(stream protobufpb.WalletService_CreditWalletServer) error {
	fmt.Println("Inside Creditwallet")

	for {
		r, err := stream.Recv()
		if r == nil {
			fmt.Println("Insdie Credit wallet, request is", r)
			break
		} else if err != nil {
			fmt.Println("Inside Creditwallet::Error in stream receive:", err)
			continue
		}
		startTime := time.Now()
		fmt.Println("Transaction Start Time", startTime, "for", r.Txnid)
		fmt.Println("Inside Creditwallet::Request Data::Transaction ID:", r.Txnid, "Transaction type:", r.TransactionType)
		fmt.Println("Inside Creditwallet::Request Data::Transaction ID:", r.Txnid, "Amount is ", r.Amount)
		fmt.Println("Inside Creditwallet::Request Data::Transaction ID::", r.Txnid, "Wallet ID:", r.Walletid, "API Wallet ID", r.Apiwalletid)
		fmt.Println("Inside Creditwallet::Request Data::Transaction ID::", r.Txnid, "Amount:", r.Walletid, "Single Layer:", r.IsSl)
		creditreq := walletservice.Walletperform{}
		//response := &protobufpb.Response{}
		//Check validation
		response, checkerr := walletservice.Checkvalidationforwallet(r)
		if checkerr != nil {
			sendErr := stream.Send(response)
			if sendErr != nil {
				fmt.Println("Inside Creditwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
				continue
			}
		} else {
			//Request Prepartion for Credit
			creditreq.Is_sl = r.IsSl
			creditreq.WalletId = r.Walletid
			creditreq.ApiWalletId = r.Apiwalletid
			creditreq.Amount = r.Amount
			creditreq.TxnId = r.Txnid
			creditreq.Type = 0 //For Credit
			creditreq.TransactionType = r.TransactionType
			//Conn acquire for insert
			acq1 := time.Now()
			conn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside Creditwallet:Exception in conn acquire:insert wallet", err.Error())
			}
			acq1end := time.Since(acq1)
			fmt.Println("Time for acquiring conn For Initiation", acq1end, "for", r.Txnid)
			//Insert Wallet ledger
			creditreq, err1 := walletservice.InsertWalletledger(context.Background(), conn, creditreq)
			conn.Release()
			if err1 == nil {
				//If Insert success then  going for credit
				conn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Inside Creditwallet:Excption in conn acquire:update wallet", err.Error())
				}
				err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
					err := updateWalletCredit(context.Background(), tx, creditreq)
					return err
				})
				conn.Release()
				if err == nil {
					response.Txnid = r.Txnid
					response.Walletstatus = "SUCCESS"
					response.Walletstatusdesc = "Successfully credited"
					response.Wallettransactionid = creditreq.Id
					response.Apiwallettransactionid = creditreq.ApiId
					response.Walletstatuscode = 0
					fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", r.Txnid, ":Status:", response.Walletstatus)
					transactionend := time.Since(startTime)
					fmt.Println("total transaction time", transactionend, "for", r.Txnid)
				} else if err != nil {
					fmt.Println("Inside Creditwallet::Error::Transaction ID::", r.Txnid, ":Failure Status desc:", err.Error())
					response.Txnid = r.Txnid
					response.Wallettransactionid = creditreq.Id
					response.Apiwallettransactionid = creditreq.ApiId
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err.Error()
					response.Walletstatuscode = 7

					creditreq.Status = response.Walletstatus
					creditreq.StatusCode = response.Walletstatuscode

					updateconn, connerr := s.Conn.Acquire(context.Background())
					if connerr != nil {
						fmt.Println("Txn id in request for credit::", r.Txnid, "Error in conn acquire in update :", connerr.Error())
					}
					walletservice.UpdateStatusCode(creditreq, updateconn)
					updateconn.Release()

					fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", r.Txnid, ":Status:", response.Walletstatus)
					fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", r.Txnid, ":Failure Status desc:", err.Error(), "Description:", response.Walletstatusdesc)
				}
				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Creditwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
					continue
				}
			} else {
				fmt.Println("error: ", err1)
				response.Txnid = r.Txnid
				response.Wallettransactionid = creditreq.Id
				response.Apiwallettransactionid = creditreq.ApiId
				response.Walletstatus = "FAILED"
				switch err1.Error() {
				case "1":
					response.Walletstatusdesc = "API Wallet ID is not available"
					response.Walletstatuscode = 2
				case "2":
					response.Walletstatusdesc = "Wallet ID is not available"
					response.Walletstatuscode = 2
				case "3":
					response.Walletstatusdesc = "Wallet ID is not active"
					response.Walletstatuscode = 1
				case "4":
					response.Walletstatusdesc = "API wallet ID is not active"
					response.Walletstatuscode = 1
				case "5":
					response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
					response.Walletstatuscode = 1
				case "9":
					response.Walletstatusdesc = "Maximum Balance Limit exceed of API user"
					response.Walletstatuscode = 5
				case "10":
					response.Walletstatusdesc = "Maximum Balance Limit exceed of user"
					response.Walletstatuscode = 5
				default:
					response.Walletstatusdesc = err1.Error()
					response.Walletstatuscode = 6
				}
				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Creditwallet::Stream send error::", r.Txnid, ":Failure desc:", sendErr)
					continue
				}
			}

		}

	}
	return nil
}
