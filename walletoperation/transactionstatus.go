package walletoperation

import (
	"context"
	"errors"
	"fmt"
	protobufpbstatus "grpcwallet/src/protobuf/protobufpbstatus"
	"grpcwallet/walletservice"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Wallet_req struct {
	Id         int64
	Status     string
	Type       int
	Statuscode int32
	Amount     float64
	ApiId      int64
}

func (s *Server) Statusquery(req *protobufpbstatus.StatusqueryRequest, stream protobufpbstatus.StatusService_StatusqueryServer) error {
	conn := s.Conn
	rowCount := 0
	res := &protobufpbstatus.StatusqueryResponse{}
	fmt.Println("Inside Status Query , request is :", req)
	response, checkerr := walletservice.Checkvalidationforcommissionstatusquery(req)
	if checkerr != nil {
		sendErr := stream.Send(response)
		if sendErr != nil {
			fmt.Println("Inside Satus Query::Stream send error::", req.Txnid, ":Failure desc:", sendErr)
		}
	}
	rows, err := conn.Query(context.Background(), "select id,walletid,status,type,status_code from wallet_ledger where txnid= $1 AND walletid= $2 UNION select id,walletid,status,type,status_code from wallet_api_ledger where txnid= $1 AND walletid= $2 ;", req.Txnid, req.Walletid)
	if err != nil {
		fmt.Println("Error:", err)
		res.Txnid = req.GetTxnid()
		res.Walletid = req.GetWalletid()
		res.WalletStatusDesc = "Exception in database"
		res.Status = "FAILED"
		stream.Send(res)
		return nil
	}
	defer rows.Close()
	for rows.Next() {
		rowCount++
		data, err1 := rows.Values()
		if err1 != nil {
			fmt.Println("error:", err1)
		}
		fmt.Println("rows data:", data, err1)
		res.Txnid = req.GetTxnid()
		res.Walletid = req.GetWalletid()
		res.WalletStatusDesc = "Transaction id AND Walletid found"
		if err := rows.Scan(&res.Id, &res.Walletid, &res.Status, &res.Type, &res.StatusCode); err != nil {
			fmt.Println("Error:", err)
			res.Txnid = req.GetTxnid()
			res.Walletid = req.GetWalletid()
			res.WalletStatusDesc = "Exception in database"
			res.Status = "FAILED"
		}
		fmt.Println("Response is: ", res)
		stream.Send(res)
	}
	if rowCount == 0 {
		fmt.Println("Transaction id is not present")
		res.Txnid = req.GetTxnid()
		res.Walletid = req.GetWalletid()
		res.WalletStatusDesc = "Transaction id OR walletid not available "
		res.Status = "FAILED"
		res.StatusCode = -1
		stream.Send(res)
	}
	return nil
}

func Status_q(walletreq walletservice.Walletperform, Conn *pgxpool.Pool) (Wallet_req, error) {
	var Reqbody Wallet_req
	//var Allreq []Wallet_req
	fmt.Println("enter for status enquiry")

	//rowCount := 0
	if walletreq.TxnId == 0 || (walletreq.WalletId == 0 && walletreq.ApiWalletId == 0) { //return
		fmt.Println("Error in validarion as Transaction id and walletid can not be zero", "Transactionid----->", walletreq.TxnId)
		return Reqbody, errors.New("Error in validarion as Transaction id and walletid can not be zero")
	}
	conn, err := Conn.Acquire(context.Background())
	if err != nil {
		fmt.Println("Inside Creditwallet:Exception in conn acquire:insert wallet", err.Error())
	}

	if walletreq.ApiWalletId == 0 && walletreq.Is_sl { //is_sl true only walletid available//Only for retilaer layer
		err := conn.QueryRow(context.Background(), "select id,status,type,status_code,amount from wallet_ledger where txnid= $1 AND walletid= $2", walletreq.TxnId, walletreq.WalletId).Scan(&Reqbody.Id, &Reqbody.Status, &Reqbody.Type, &Reqbody.Statuscode, &Reqbody.Amount)
		if err != nil {
			fmt.Println("Select error for wallet_ledger", err.Error())
			if err.Error() == "no rows in result set" { //error when data is not available
				conn.Release()
				return Reqbody, nil
			} else {
				conn.Release()
				return Reqbody, err
			}
		} else {
			conn.Release()
			return Reqbody, nil

		}

	} else if walletreq.WalletId == 0 && walletreq.Is_sl { //is_sl true only api_walletid available
		err := conn.QueryRow(context.Background(), "select id,status,type,status_code,amount from wallet_api_ledger where txnid= $1 AND walletid= $2", walletreq.TxnId, walletreq.ApiWalletId).Scan(&Reqbody.ApiId, &Reqbody.Status, &Reqbody.Type, &Reqbody.Statuscode, &Reqbody.Amount)
		if err != nil {
			fmt.Println("Select error for wallet_ledger", err.Error())
			if err.Error() == "no rows in result set" { //error when data is not available
				conn.Release()
				return Reqbody, nil
			} else {
				conn.Release()
				return Reqbody, err
			}

		} else {
			conn.Release()
			return Reqbody, nil
		}

	} else { //is_sl false

		err := conn.QueryRow(context.Background(), "select id,status,type,status_code,amount from wallet_ledger where txnid= $1 AND walletid= $2", walletreq.TxnId, walletreq.WalletId).Scan(&Reqbody.Id, &Reqbody.Status, &Reqbody.Type, &Reqbody.Statuscode, &Reqbody.Amount)

		err1 := conn.QueryRow(context.Background(), "select id,status,type,status_code,amount from wallet_api_ledger where txnid= $1 AND walletid= $2", walletreq.TxnId, walletreq.ApiWalletId).Scan(&Reqbody.ApiId, &Reqbody.Status, &Reqbody.Type, &Reqbody.Statuscode, &Reqbody.Amount)

		// }
		fmt.Println("error", err)
		if err != nil || err1 != nil {
			fmt.Println("Select error for wallet_ledger", err.Error())
			if err.Error() == "no rows in result set" || err1.Error() == "no rows in result set" { //review
				conn.Release()
				return Reqbody, nil
			} else {
				conn.Release()
				return Reqbody, err
			}

		} else {
			conn.Release()
			return Reqbody, nil
		}
	}

	//return Reqbody, err

}
