package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func DebitpendingWallet(Conn *pgxpool.Pool, req walletservice.Walletperform, Url string, resp Apiresp) (Apiresp, error) {
	fmt.Println("Inside PendingDebitwallet")

	//debitreq := walletservice.Walletperform{}

	fmt.Println("Txn id in request for debit: ", req.TxnId, "Wallet ID:", req.WalletId, "Api Wallet ID:", req.ApiWalletId)
	fmt.Println("Txn id in request for debit: ", req.TxnId, "of amount:", req.Amount)
	fmt.Println("Txn id in request for debit: ", req.TxnId, "Getting user transaction layer: ", req.Is_sl, "Txn Type", req.TransactionType)
	//Check for Requested data is valid or not
	request := &protobufpb.Request{}
	request.Txnid = req.TxnId
	request.Walletid = req.WalletId
	request.Apiwalletid = req.ApiWalletId
	request.IsSl = req.Is_sl
	request.TransactionType = req.TransactionType
	request.Amount = req.Amount
	response, checkerr := walletservice.Checkvalidationforwallet(request)
	if checkerr != nil {
		resp.Walletstatus = response.Walletstatus
		resp.Walletstatuscode = response.Walletstatuscode
		resp.Walletstatusdesc = response.Walletstatusdesc
		resp.Wallettransactionid = response.Wallettransactionid
		resp.Apiwallettransactionid = response.Apiwallettransactionid
		fmt.Println("error in validation", checkerr)
		return resp, checkerr
	} else {
		//var resp Apiresp
		conn, err := Conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Txn id in request for debit::", req.TxnId, "Error in conn acquire in insert :", err.Error())
		}
		//insert service for initiated transaction of wallet ledger
		debitreq, err1 := walletservice.InsertWalletledger(context.Background(), conn, req)
		conn.Release()
		if err1 == nil {
			//If insert data in ledger success
			conn, err := Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Txn id in request for debit::", req.TxnId, "Error in conn acquire in update :", err.Error())
			}
			err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
				conn.Exec(context.Background(), "SET TRANSACTION PRIORITY HIGH")
				fmt.Println("Going for update ")
				err := updatewalletdebit(context.Background(), tx, debitreq)
				return err
			})
			conn.Release()
			if err == nil {
				response.Txnid = request.Txnid
				response.Walletstatus = "SUCCESS"
				response.Walletstatusdesc = "Successfully Debited"
				response.Wallettransactionid = debitreq.Id
				response.Apiwallettransactionid = debitreq.ApiId
				response.Walletstatuscode = 0
				fmt.Println("Txn id in request for debit: ", request.Txnid, "status is :", response.Walletstatus)

			} else if err != nil {
				fmt.Println("Txn id in request for debit: ", request.Txnid, "exception in update :", err)
				response.Txnid = request.Txnid
				response.Wallettransactionid = debitreq.Id
				response.Apiwallettransactionid = debitreq.ApiId
				switch err.Error() {
				case "5":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Minimum balance not available"
					response.Walletstatuscode = 3
				case "6":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "API minimum balance not available"
					response.Walletstatuscode = 3
				case "7":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Balance not available"
					response.Walletstatuscode = 4
				case "8":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "API balance not available"
					response.Walletstatuscode = 4

				default:
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err.Error()
					response.Walletstatuscode = 7
				}

				debitreq.Status = response.Walletstatus
				debitreq.StatusCode = response.Walletstatuscode
				conn, err := Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Txn id in request for debit::", req.TxnId, "Error in conn acquire in update :", err.Error())
				}
				walletservice.UpdateStatusCode(debitreq, conn)
				conn.Release()
			}

		} else if err1 != nil {
			response.Txnid = req.TxnId
			response.Wallettransactionid = debitreq.Id
			response.Apiwallettransactionid = debitreq.ApiId
			switch err1.Error() {
			case "1":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "API Wallet ID is not available"
				response.Walletstatuscode = 2
			case "2":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID is not available"
				response.Walletstatuscode = 2
			case "3":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID is not active"
				response.Walletstatuscode = 1
			case "4":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "API wallet ID is not active"
				response.Walletstatuscode = 1
			case "5":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
				response.Walletstatuscode = 1
			default:
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = err1.Error()
				response.Walletstatuscode = 6
			}

		}

	}
	resp.Walletstatus = response.Walletstatus
	resp.Walletstatuscode = response.Walletstatuscode
	resp.Walletstatusdesc = response.Walletstatusdesc
	resp.Wallettransactionid = response.Wallettransactionid
	resp.Apiwallettransactionid = response.Apiwallettransactionid
	return resp, nil
}
