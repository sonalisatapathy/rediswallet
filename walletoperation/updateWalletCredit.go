package walletoperation

import (
	"context"
	"errors"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func updateWalletCredit(ctx context.Context, tx pgx.Tx, creditreq walletservice.Walletperform) error {
	fmt.Println("Txn id in request for update credit: ", creditreq.Id, "Wallet ID:", creditreq.WalletId, "Api Wallet ID:", creditreq.ApiWalletId)
	fmt.Println("Txn id in request for update credit: ", creditreq.Id, "of amount:", creditreq.Amount)
	fmt.Println("Txn id in request for update credit: ", creditreq.Id, "Getting user transaction layer: ", creditreq.Is_sl)
	//For single layer Credit for api user
	if creditreq.Is_sl && creditreq.ApiWalletId != 0 {
		//Check balance and maximum balance for wallet credit for API
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", creditreq.ApiWalletId).Scan(&creditreq.ApiPreviousBalance, &creditreq.ApimaximumBalnce); err != nil {
			return err
		}
		//Return if Reached maximum balance
		if creditreq.ApiPreviousBalance+creditreq.Amount >= creditreq.ApimaximumBalnce {
			return errors.New("9")
		}
		//Credit amount from API wallet
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.ApiWalletId); err != nil {
			return err
		}
		creditreq.ApiCurrentBalance = creditreq.ApiPreviousBalance + creditreq.Amount
		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		//Update API wallet ledger after credit
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set PreviousBalance = $2 , CurrentBalance = $3 ,status= $4,updateddate = $5,status_code=$6 where id = $1", creditreq.ApiId, creditreq.ApiPreviousBalance, creditreq.ApiCurrentBalance, "SUCCESS", creditreq.UpdatedDate, creditreq.RefundStatusCode); err != nil {
			return err
		}
		creditreq.Status = "SUCCESS"
		creditreq.StatusCode = int32(creditreq.RefundStatusCode)
		go walletservice.PubsubData(creditreq)
		return nil
	} else if creditreq.Is_sl && creditreq.WalletId != 0 {
		//For single layer Credit for retailer user
		//Check balance
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet WHERE id = $1 FOR UPDATE", creditreq.WalletId).Scan(&creditreq.PreviousBalance, &creditreq.MaximumBalance); err != nil {
			return err
		}
		//Check maximum balance
		if creditreq.PreviousBalance+creditreq.Amount >= creditreq.MaximumBalance {
			return errors.New("10")
		}
		//update wallet for credit
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.WalletId); err != nil {
			return err
		}
		creditreq.CurrentBalance = creditreq.PreviousBalance + creditreq.Amount
		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		//Update wallet ledger for credit
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set PreviousBalance = $2 , CurrentBalance = $3 ,status= $4,updateddate =$5,status_code=$6 where id = $1", creditreq.Id, creditreq.PreviousBalance, creditreq.CurrentBalance, "SUCCESS", creditreq.UpdatedDate, creditreq.RefundStatusCode); err != nil {
			return err
		}
		creditreq.Status = "SUCCESS"
		creditreq.StatusCode = int32(creditreq.RefundStatusCode)
		go walletservice.PubsubData(creditreq)

		return nil
	} else if !creditreq.Is_sl && creditreq.WalletId != 0 && creditreq.ApiWalletId != 0 {
		//select for balance,max balance for wallet
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet WHERE id = $1 FOR UPDATE", creditreq.WalletId).Scan(&creditreq.PreviousBalance, &creditreq.MaximumBalance); err != nil {
			return err
		}
		//Check for  max balance limit
		if creditreq.PreviousBalance+creditreq.Amount >= creditreq.MaximumBalance {
			return errors.New("10")
		}
		//select for balance,max balance for wallet api
		if err := tx.QueryRow(ctx,
			"SELECT balance,maximum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", creditreq.ApiWalletId).Scan(&creditreq.ApiPreviousBalance, &creditreq.ApimaximumBalnce); err != nil {
			return err
		}
		//Check for max balance limit
		if creditreq.ApiPreviousBalance+creditreq.Amount >= creditreq.ApimaximumBalnce {
			return errors.New("9")
		}

		//Update  wallet
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.WalletId); err != nil {
			return err
		}
		creditreq.CurrentBalance = creditreq.PreviousBalance + creditreq.Amount
		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		//Update wallet ledger
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set PreviousBalance = $2 , CurrentBalance = $3 ,status= $4,updateddate =$5,status_code=$6 where id = $1", creditreq.Id, creditreq.PreviousBalance, creditreq.CurrentBalance, "SUCCESS", creditreq.UpdatedDate, creditreq.RefundStatusCode); err != nil {
			return err
		}
		//Update wallet api
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance + $1 WHERE id = $2", creditreq.Amount, creditreq.ApiWalletId); err != nil {
			return err
		}
		creditreq.ApiCurrentBalance = creditreq.ApiPreviousBalance + creditreq.Amount
		//Update wallet api ledger
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set PreviousBalance = $2 , CurrentBalance = $3 ,status= $4,updateddate = $5,status_code=$6 where id = $1", creditreq.ApiId, creditreq.ApiPreviousBalance, creditreq.ApiCurrentBalance, "SUCCESS", creditreq.UpdatedDate, creditreq.RefundStatusCode); err != nil {
			return err
		}
		creditreq.Status = "SUCCESS"
		creditreq.StatusCode = int32(creditreq.RefundStatusCode)
		go walletservice.PubsubData(creditreq)
		return nil
	} else {
		return errors.New("wrong data")
	}

}
func updatestatus(ctx context.Context, conn *pgxpool.Conn, r *protobufpb.Refundrequest, creditreq walletservice.Walletperform) error {
	fmt.Println("enter for update statuscode")
	creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")

	if _, err := conn.Exec(context.Background(),
		"UPDATE wallet_ledger set status_code=6,updateddate =$2 where txnid = $1 and type=1", r.Txnid, creditreq.UpdatedDate); err != nil {
		return err
	}
	creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
	if _, err := conn.Exec(context.Background(),
		"UPDATE wallet_api_ledger set status_code=6,updateddate=$2 where txnid = $1 and type=1", r.Txnid, creditreq.UpdatedDate); err != nil {
		return err
	}
	var debitreq walletservice.Walletperform
	createdtime := time.Now()
	updatedtime := time.Now()

	if err := conn.QueryRow(ctx,
		"SELECT * FROM wallet_ledger WHERE txnid = $1", r.Txnid).Scan(&debitreq.Id, &debitreq.PreviousBalance, &debitreq.Amount, &debitreq.CurrentBalance, &debitreq.WalletId, &debitreq.Status, &debitreq.Type, &debitreq.ReversalId, &debitreq.StatusCode, &debitreq.TxnId, &createdtime, &updatedtime, &debitreq.TransactionType); err != nil {
		return err
	}
	if err := conn.QueryRow(ctx,
		"SELECT * FROM wallet_api_ledger WHERE txnid = $1", r.Txnid).Scan(&debitreq.ApiId, &debitreq.ApiPreviousBalance, &debitreq.Amount, &debitreq.ApiCurrentBalance, &debitreq.ApiWalletId, &debitreq.Status, &debitreq.Type, &debitreq.ApiReversalId, &debitreq.StatusCode, &debitreq.TxnId, &createdtime, &updatedtime, &debitreq.TransactionType); err != nil {
		return err
	}
	//debitreq.StatusCode = int32(creditreq.RefundStatusCode)
	//fmt.Println("date", createdtime) //IST

	debitreq.CreatedDate = createdtime.UTC().Format("2006-01-02 15:04:05.000000")
	debitreq.UpdatedDate = updatedtime.UTC().Format("2006-01-02 15:04:05.000000")
	go walletservice.PubsubData(debitreq)
	return nil
}
