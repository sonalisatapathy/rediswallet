package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
)

func (s *Server)StatusQuery(req *protobufpb.StatusQueryRequest, stream protobufpb.WalletService_StatusQueryServer) error {

	conn := s.Conn
	rowCount := 0

	res := &protobufpb.StatusQueryResponse{}
	fmt.Println("Inside Status Query , request is :", req)

	response, checkerr := walletservice.Checkvalidationforstatusquery(req)

	if checkerr != nil {
		sendErr := stream.Send(response)
		if sendErr != nil {
			fmt.Println("Inside Satus Query::Stream send error::", req.Txnid, ":Failure desc:", sendErr)

		}
	}

	rows, err := conn.Query(context.Background(), "select id,previousbalance,amount,currentbalance,walletid,status,type,status_code from wallet_ledger where txnid= $1 UNION select id,previousbalance,amount,currentbalance,walletid,status,type,status_code from wallet_api_ledger where txnid= $1;", req.Txnid)
	if err != nil {
		fmt.Println("Error:", err)
		res.Txnid = req.GetTxnid()
		res.WalletStatusDesc = "Exception in database"
		res.Status = "FAILED"
		stream.Send(res)
		return nil

	}

	defer rows.Close()

	for rows.Next() {

		rowCount++
		data, err1 := rows.Values()
		if err1 != nil {
			fmt.Println("error:", err1)
		}
		fmt.Println("rows data:", data, err1)
		res.Txnid = req.GetTxnid()
		res.WalletStatusDesc = "Transaction id found"
		if err := rows.Scan(&res.Id, &res.PreviousBalance, &res.Amount, &res.CurrentBalance, &res.Walletid, &res.Status, &res.Type, &res.StatusCode); err != nil {
			fmt.Println("Error:", err)
			res.Txnid = req.GetTxnid()
			res.WalletStatusDesc = "Exception in database"
			res.Status = "FAILED"
			//zapLogger.Info(err.Error())
		}

		fmt.Println("Response is: ", res)
		stream.Send(res)
	}

	if rowCount == 0 {
		fmt.Println("Transaction id is not present")
		res.Txnid = req.GetTxnid()
		res.WalletStatusDesc = "Transaction id not available"
		res.Status = "FAILED"
		res.StatusCode = -1
		stream.Send(res)

	}

	return nil
}
