package walletoperation

import (
	"context"
	"errors"
	"fmt"
	"grpcwallet/walletservice"
	"time"

	"github.com/jackc/pgx/v4"
)

func updatewalletdebit(ctx context.Context, tx pgx.Tx, debitreq walletservice.Walletperform) error {
	issl := debitreq.Is_sl
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "Wallet ID:", debitreq.WalletId, "Api Wallet ID:", debitreq.ApiWalletId)
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "of amount:", debitreq.Amount)
	fmt.Println("Txn id in request for update debit: ", debitreq.Id, "Getting user transaction layer: ", debitreq.Is_sl)
	if issl && debitreq.ApiWalletId != 0 {
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", debitreq.ApiWalletId).Scan(&debitreq.ApiPreviousBalance, &debitreq.ApiminimumBalance); err != nil {
			return err
		}
		if debitreq.ApiPreviousBalance < debitreq.Amount {
			return errors.New("8")
		} else if debitreq.ApiPreviousBalance-debitreq.Amount < debitreq.ApiminimumBalance {
			return errors.New("6")
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.ApiWalletId); err != nil {
			return err
		}
		debitreq.ApiCurrentBalance = debitreq.ApiPreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.ApiId, debitreq.ApiPreviousBalance, debitreq.ApiCurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		return nil
	} else if issl && debitreq.WalletId != 0 {

		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet WHERE id = $1 FOR UPDATE", debitreq.WalletId).Scan(&debitreq.PreviousBalance, &debitreq.MinimumBalance); err != nil {
			return err
		}
		fmt.Println("pr", debitreq.PreviousBalance, debitreq.MinimumBalance)
		if debitreq.PreviousBalance < debitreq.Amount {
			return errors.New("7")
		} else if debitreq.PreviousBalance-debitreq.Amount < debitreq.MinimumBalance {
			return errors.New("5")
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.WalletId); err != nil {
			return err
		}
		debitreq.CurrentBalance = debitreq.PreviousBalance - debitreq.Amount
		fmt.Println("crnt bal", debitreq.CurrentBalance)
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate=$5 where id = $1", debitreq.Id, debitreq.PreviousBalance, debitreq.CurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		return nil
	} else if !issl && debitreq.WalletId != 0 && debitreq.ApiWalletId != 0 {
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet WHERE id = $1 FOR UPDATE", debitreq.WalletId).Scan(&debitreq.PreviousBalance, &debitreq.MinimumBalance); err != nil {
			return err
		}
		if err := tx.QueryRow(ctx,
			"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1 FOR UPDATE", debitreq.ApiWalletId).Scan(&debitreq.ApiPreviousBalance, &debitreq.ApiminimumBalance); err != nil {
			return err
		}
		if debitreq.PreviousBalance < debitreq.Amount {
			return errors.New("7")
		} else if debitreq.ApiPreviousBalance < debitreq.Amount {
			return errors.New("8")
		} else if (debitreq.PreviousBalance - debitreq.Amount) < debitreq.MinimumBalance {
			return errors.New("5")
		} else if (debitreq.ApiPreviousBalance - debitreq.Amount) < debitreq.ApiminimumBalance {
			return errors.New("6")
		}

		if _, err := tx.Exec(ctx,
			"UPDATE wallet SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.WalletId); err != nil {
			return err
		}
		debitreq.CurrentBalance = debitreq.PreviousBalance - debitreq.Amount
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.Id, debitreq.PreviousBalance, debitreq.CurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			fmt.Println("Exception in wallet ledger data ")
			return err
		}
		if _, err := tx.Exec(ctx,
			"UPDATE wallet_api SET balance = balance - $1 WHERE id = $2", debitreq.Amount, debitreq.ApiWalletId); err != nil {
			return err
		}
		debitreq.ApiCurrentBalance = debitreq.ApiPreviousBalance - debitreq.Amount
		if _, err := tx.Exec(context.Background(),
			"UPDATE wallet_api_ledger set previousbalance = $2 , currentbalance = $3 ,status= $4,updateddate = $5 where id = $1", debitreq.ApiId, debitreq.ApiPreviousBalance, debitreq.ApiCurrentBalance, "SUCCESS", debitreq.UpdatedDate); err != nil {
			return err
		}
		debitreq.Status = "SUCCESS"
		go walletservice.PubsubData(debitreq)
		return nil
	} else {
		return errors.New("wrong data")
	}

}
