package walletoperation

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

func (s *Server) DebitWallet(stream protobufpb.WalletService_DebitWalletServer) error {
	fmt.Println("Inside Debitwallet")
	for {
		//stream data received for debit wallet
		req, err := stream.Recv()
		if req == nil {
			//If stream data receive nil in request break this stream
			fmt.Println("Insdie Debit wallet, request is", req)
			break
		} else if err != nil {
			//If error occured in receiving stream data
			fmt.Println("Inside Debitwallet::Error in stream receive:", err)
			continue
		}
		startTime := time.Now()
		fmt.Println("Transaction Start Time", startTime, "for", req.Txnid)
		debitreq := walletservice.Walletperform{}
		fmt.Println("Txn id in request for debit: ", req.Txnid, "Wallet ID:", req.Walletid, "Api Wallet ID:", req.Apiwalletid)
		fmt.Println("Txn id in request for debit: ", req.Txnid, "of amount:", req.Amount)
		fmt.Println("Txn id in request for debit: ", req.Txnid, "Getting user transaction layer: ", req.IsSl, "Txn Type", req.TransactionType)
		//Check for Requested data is valid or not
		response, checkerr := walletservice.Checkvalidationforwallet(req)
		if checkerr != nil {
			sendErr := stream.Send(response)
			if sendErr != nil {
				fmt.Println("Inside Deitwallet::Stream send error::", req.Txnid, ":Failure desc:", sendErr)
				continue
			}
		} else {
			//Request Prepartion for debit wallet
			debitreq.Is_sl = req.IsSl
			debitreq.WalletId = req.Walletid
			debitreq.ApiWalletId = req.Apiwalletid
			debitreq.Amount = req.Amount
			debitreq.TxnId = req.Txnid
			debitreq.Type = 1 //For Debit
			debitreq.TransactionType = req.TransactionType
			//Conn acquire for insert
			acq1 := time.Now()
			intconn, err := s.Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Txn id in request for debit::", req.GetTxnid(), "Error in conn acquire in insert :", err.Error())
				continue
			}
			acq1end := time.Since(acq1)
			fmt.Println("Time for acquiring conn For Initiation", acq1end, "for", req.Txnid)
			//insert service for initiated transaction of wallet ledger
			debitreq, err1 := walletservice.InsertWalletledger(context.Background(), intconn, debitreq)
			//Conn release
			intconn.Release()
			if err1 == nil {
				//If insert data in ledger success
				conn, err := s.Conn.Acquire(context.Background())
				if err != nil {
					fmt.Println("Txn id in request for debit::", req.GetTxnid(), "Error in conn acquire in update :", err.Error())
				}
				err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
					conn.Exec(context.Background(), "SET TRANSACTION PRIORITY HIGH")
					err := updatewalletdebit(context.Background(), tx, debitreq)
					return err
				})
				conn.Release()
				if err == nil {
					response.Txnid = req.Txnid
					response.Walletstatus = "SUCCESS"
					response.Walletstatusdesc = "Successfully Debited"
					response.Wallettransactionid = debitreq.Id
					response.Apiwallettransactionid = debitreq.ApiId
					response.Walletstatuscode = 0
					fmt.Println("Txn id in request for debit: ", req.Txnid, "status is :", response.Walletstatus)
					transactionend := time.Since(startTime)
					fmt.Println("total transaction time", transactionend, "for", req.Txnid)
					//insert in redis
					//cdc
				} else if err != nil {
					fmt.Println("Txn id in request for debit: ", req.Txnid, "exception in update :", err)
					response.Txnid = req.GetTxnid()
					response.Wallettransactionid = debitreq.Id
					response.Apiwallettransactionid = debitreq.ApiId
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err.Error()
					response.Walletstatuscode = 7
					debitreq.Status = response.Walletstatus
					debitreq.StatusCode = response.Walletstatuscode

					sendErr := stream.Send(response)
					if sendErr != nil {
						fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
						continue
					}
					updateconn, err := s.Conn.Acquire(context.Background())
					if err != nil {
						fmt.Println("Txn id in request for debit::", req.GetTxnid(), "Error in conn acquire in update :", err.Error())
					}
					walletservice.UpdateStatusCode(debitreq, updateconn)
					updateconn.Release()
					continue
				}
				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
					continue
				}

			} else if err1 != nil {
				response.Txnid = req.Txnid
				response.Wallettransactionid = debitreq.Id
				response.Apiwallettransactionid = debitreq.ApiId
				response.Walletstatus = "FAILED"
				switch err1.Error() {
				case "1":
					response.Walletstatusdesc = "API Wallet ID is not available"
					response.Walletstatuscode = 2
				case "2":
					response.Walletstatusdesc = "Wallet ID is not available"
					response.Walletstatuscode = 2
				case "3":
					response.Walletstatusdesc = "Wallet ID is not active"
					response.Walletstatuscode = 1
				case "4":
					response.Walletstatusdesc = "API wallet ID is not active"
					response.Walletstatuscode = 1
				case "5":
					response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
					response.Walletstatuscode = 1
				case "6":
					response.Walletstatusdesc = "API minimum balance not available"
					response.Walletstatuscode = 3
				case "7":
					response.Walletstatusdesc = "Balance not available"
					response.Walletstatuscode = 4
				case "8":
					response.Walletstatusdesc = "API balance not available"
					response.Walletstatuscode = 4
				case "11":
					response.Walletstatusdesc = "Minimum balance not available"
					response.Walletstatuscode = 3

				default:
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err1.Error()
					response.Walletstatuscode = 6
				}

				sendErr := stream.Send(response)
				if sendErr != nil {
					fmt.Println("Inside Debitwallet::Stream send error::", response.Txnid, ":Failure desc:", sendErr)
					continue
				}

			}

		}

	}
	return nil
}
