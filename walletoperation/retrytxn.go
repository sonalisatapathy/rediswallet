package walletoperation

import (
	"context"
	"fmt"
	"grpcwallet/walletservice"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func Initiated(walletreq walletservice.Walletperform, Conn *pgxpool.Pool, Url string, Resp Apiresp) (Apiresp, error) {
	//var Resp Apiresp
	var errdebit error

	if walletreq.Type == 1 {
		//	var err error
		fmt.Println("debit operation for initiated case")
		conn, _ := Conn.Acquire(context.Background())
		errdebit = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
			conn.Exec(context.Background(), "SET TRANSACTION PRIORITY HIGH")
			err := updatewalletdebit(context.Background(), tx, walletreq)
			return err
		})
		conn.Release()
		if errdebit == nil {
			Resp.Txnid = walletreq.TxnId
			Resp.Walletstatus = "SUCCESS"
			Resp.Walletstatusdesc = "Successfully Debited"
			Resp.Wallettransactionid = walletreq.Id
			Resp.Apiwallettransactionid = walletreq.ApiId
			Resp.Walletstatuscode = 0
			fmt.Println("Txn id in request for debit: ", walletreq.TxnId, "status is :", Resp.Walletstatus)
		} else if errdebit != nil {
			fmt.Println("Txn id in request for debit: ", walletreq.TxnId, "exception in update :", errdebit)
			Resp.Txnid = walletreq.TxnId
			Resp.Wallettransactionid = walletreq.Id
			Resp.Apiwallettransactionid = walletreq.ApiId
			switch errdebit.Error() {
			case "5":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "Minimum balance not available"
				Resp.Walletstatuscode = 3
			case "6":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "API minimum balance not available"
				Resp.Walletstatuscode = 3
			case "7":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "Balance not available"
				Resp.Walletstatuscode = 4
			case "8":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "API balance not available"
				Resp.Walletstatuscode = 4
			default:
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = errdebit.Error()
				Resp.Walletstatuscode = 7
			}
			walletreq.Status = Resp.Walletstatus
			walletreq.StatusCode = Resp.Walletstatuscode
			updateconn, err := Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Txn id in request for debit::", walletreq.TxnId, "Error in conn acquire in update :", err.Error())
			}
			walletservice.UpdateStatusCode(walletreq, updateconn)
			updateconn.Release()
		}

		return Resp, nil

	} else if walletreq.Type == 0 {
		var deberr error
		fmt.Println("credit operation")
		conn, _ := Conn.Acquire(context.Background())
		deberr = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
			err := updateWalletCredit(context.Background(), tx, walletreq)
			return err
		})
		conn.Release()
		if deberr == nil {
			Resp.Txnid = walletreq.TxnId
			Resp.Walletstatus = "SUCCESS"
			Resp.Walletstatusdesc = "Successfully Credited"
			Resp.Wallettransactionid = walletreq.Id
			Resp.Apiwallettransactionid = walletreq.ApiId
			Resp.Walletstatuscode = 0
			fmt.Println("Txn id in request for Credit: ", walletreq.TxnId, "status is :", Resp.Walletstatus)
		} else if deberr != nil {
			fmt.Println("Txn id in request for Credit: ", walletreq.TxnId, "exception in update :", deberr)
			Resp.Txnid = walletreq.TxnId
			Resp.Wallettransactionid = walletreq.Id
			Resp.Apiwallettransactionid = walletreq.ApiId
			switch deberr.Error() {
			case "9":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "Maximum Balnce Limit exceed of API user"
				Resp.Walletstatuscode = 5
			case "10":
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = "Maximum Balance Limit exceed of user"
				Resp.Walletstatuscode = 5
			default:
				Resp.Walletstatus = "FAILED"
				Resp.Walletstatusdesc = deberr.Error()
				Resp.Walletstatuscode = 7
			}
			walletreq.Status = Resp.Walletstatus
			walletreq.StatusCode = Resp.Walletstatuscode
			updateconn, err := Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Txn id in request for credit::", walletreq.TxnId, "Error in conn acquire in update :", err.Error())
			}
			walletservice.UpdateStatusCode(walletreq, updateconn)
			updateconn.Release()
		}

		return Resp, nil

	}
	return Resp, nil
}
func Walletop(walletreq walletservice.Walletperform, Conn *pgxpool.Pool, Url string, Resp Apiresp) (Apiresp, error) {

	var err error
	if walletreq.Type == 1 {
		fmt.Println("issl", walletreq.Is_sl)
		Resp, err = DebitpendingWallet(Conn, walletreq, Url, Resp)
		fmt.Println("statuscode", Resp.Walletstatuscode)
		if err != nil {
			Resp.Txnid = walletreq.TxnId
			Resp.Walletstatus = "FAILED"
			Resp.Walletstatusdesc = "ERROR" + err.Error()
			Resp.Wallettransactionid = Resp.Wallettransactionid
			Resp.Apiwallettransactionid = Resp.Apiwallettransactionid
			Resp.Walletstatuscode = Resp.Walletstatuscode
			return Resp, err
		}

	} else if walletreq.Type == 0 {
		fmt.Println("credit")
		fmt.Println("issl", walletreq.Is_sl)
		Resp, err = PendingCreditWallet(Conn, walletreq, Url, Resp)
		if err != nil {
			Resp.Txnid = walletreq.TxnId
			Resp.Walletstatus = "FAILED"
			Resp.Walletstatusdesc = "ERROR" + err.Error()
			Resp.Wallettransactionid = Resp.Wallettransactionid
			Resp.Apiwallettransactionid = Resp.Wallettransactionid
			Resp.Walletstatuscode = Resp.Walletstatuscode
			return Resp, err

		}
	}

	return Resp, nil
}
