package walletoperation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

func Sendresp(walletresesp Apiresp, url string) (string, error) {
	fmt.Println("Wallet request : ", walletresesp)

	jsonvalue, err := json.Marshal(walletresesp)
	if err != nil {
		fmt.Println("Marshalling error of the wallet response : ", err)
	}
	u := strings.NewReader(string(jsonvalue))
	//url = "https://commissiongo-twdwtabx5q-uc.a.run.app/commission/updatestatus"
	req, err := http.NewRequest("POST", url, u)
	if err != nil {
		fmt.Print("Error setting the request to send the wallet request : ", err.Error())
		return "", err
	}
	//fmt.Println("Request :", req)
	client := &http.Client{}
	resp, err := client.Do(req)
	a := resp.Status
	fmt.Println("Statuscode  : ", a)
	if err != nil {
		fmt.Print("Error in send", err.Error())
		return a, err
	}
	defer resp.Body.Close()
	return a, err

}
