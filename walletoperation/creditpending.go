package walletoperation

import (
	"context"
	"fmt"

	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"grpcwallet/walletservice"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

func PendingCreditWallet(Conn *pgxpool.Pool, creditreq walletservice.Walletperform, Url string, resp Apiresp) (Apiresp, error) {
	fmt.Println("Inside Creditwallet")
	fmt.Println("Txn id in request for credit: ", creditreq.TxnId, "Wallet ID:", creditreq.WalletId, "Api Wallet ID:", creditreq.ApiWalletId)
	fmt.Println("Txn id in request for credit: ", creditreq.TxnId, "of amount:", creditreq.Amount)
	fmt.Println("Txn id in request for credit: ", creditreq.TxnId, "Getting user transaction layer: ", creditreq.Is_sl, "Txn Type", creditreq.TransactionType)

	//Check validation
	request := &protobufpb.Request{}
	request.Txnid = creditreq.TxnId
	request.Walletid = creditreq.WalletId
	request.Apiwalletid = creditreq.ApiWalletId
	request.IsSl = creditreq.Is_sl
	request.TransactionType = creditreq.TransactionType
	request.Amount = creditreq.Amount
	response, checkerr := walletservice.Checkvalidationforwallet(request)
	if checkerr != nil {
		resp.Walletstatus = response.Walletstatus
		resp.Walletstatuscode = response.Walletstatuscode
		resp.Walletstatusdesc = response.Walletstatusdesc
		resp.Wallettransactionid = response.Wallettransactionid
		resp.Apiwallettransactionid = response.Apiwallettransactionid
		fmt.Println("error in validation", checkerr)
		return resp, checkerr
	} else {
		fmt.Println("Checking conn")
		//Request Prepartion for Credit
		conn, err := Conn.Acquire(context.Background())
		if err != nil {
			fmt.Println("Inside Creditwallet:Exception in conn acquire:insert wallet", err.Error())
		}
		//Insert Wallet ledger
		creditreq, err1 := walletservice.InsertWalletledger(context.Background(), conn, creditreq)
		conn.Release()
		if err1 == nil {
			//If Insert success then  going for credit
			conn, err := Conn.Acquire(context.Background())
			if err != nil {
				fmt.Println("Inside Creditwallet:Excption in conn acquire:update wallet", err.Error())
			}
			err = crdbpgx.ExecuteTx(context.Background(), conn, pgx.TxOptions{}, func(tx pgx.Tx) error {
				err := updateWalletCredit(context.Background(), tx, creditreq)
				return err
			})
			conn.Release()
			if err == nil {
				response.Txnid = request.Txnid
				response.Walletstatus = "SUCCESS"
				response.Walletstatusdesc = "Successfully credited"
				response.Wallettransactionid = creditreq.Id
				response.Apiwallettransactionid = creditreq.ApiId
				response.Walletstatuscode = 0
				fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", request.Txnid, ":Status:", response.Walletstatus)
			} else if err != nil {
				fmt.Println("Inside Creditwallet::Error::Transaction ID::", request.Txnid, ":Failure Status desc:", err.Error())
				response.Txnid = request.Txnid
				response.Wallettransactionid = creditreq.Id
				response.Apiwallettransactionid = creditreq.ApiId
				switch err.Error() {
				case "9":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Maximum Balnce Limit exceed of API user"
					response.Walletstatuscode = 5
				case "10":
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = "Maximum Balance Limit exceed of user"
					response.Walletstatuscode = 5
				default:
					response.Walletstatus = "FAILED"
					response.Walletstatusdesc = err.Error()
					response.Walletstatuscode = 7
				}

				creditreq.Status = response.Walletstatus
				creditreq.StatusCode = response.Walletstatuscode

				conn, connerr := Conn.Acquire(context.Background())
				if connerr != nil {
					fmt.Println("Txn id in request for credit::", request.Txnid, "Error in conn acquire in update :", connerr.Error())
				}
				walletservice.UpdateStatusCode(creditreq, conn)
				conn.Release()

				fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", request.Txnid, ":Status:", response.Walletstatus)
				fmt.Println("Inside Creditwallet::Response Data::Transaction ID::", request.Txnid, ":Failure Status desc:", err.Error(), "Description:", response.Walletstatusdesc)
			}

		} else {
			fmt.Println("error: ", err1)
			response.Txnid = request.Txnid
			response.Wallettransactionid = creditreq.Id
			response.Apiwallettransactionid = creditreq.ApiId
			switch err1.Error() {
			case "1":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "API Wallet ID is not available"
				response.Walletstatuscode = 2
			case "2":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID is not available"
				response.Walletstatuscode = 2
			case "3":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID is not active"
				response.Walletstatuscode = 1
			case "4":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "API wallet ID is not active"
				response.Walletstatuscode = 1
			case "5":
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = "Wallet ID/API WalletID is not active"
				response.Walletstatuscode = 1
			default:
				response.Walletstatus = "FAILED"
				response.Walletstatusdesc = err1.Error()
				response.Walletstatuscode = 6
			}

		}

	}
	resp.Walletstatus = response.Walletstatus
	resp.Walletstatuscode = response.Walletstatuscode
	resp.Walletstatusdesc = response.Walletstatusdesc
	resp.Wallettransactionid = response.Wallettransactionid
	resp.Apiwallettransactionid = response.Apiwallettransactionid
	fmt.Println("Actual response", resp)
	return resp, nil
}
