package main

//wallet3test for timeout testing
import (
	"context"
	"fmt"
	"path/filepath"

	protobufpb "grpcwallet/src/protobuf/protobufpb"
	protobuftest "grpcwallet/src/protobuf/protobuftest"
	"grpcwallet/walletoperation"
	"log"
	"net"
	"time"

	"google.golang.org/grpc/keepalive"

	"github.com/jackc/pgx/v4/pgxpool"

	"github.com/gomodule/redigo/redis"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var conn *pgxpool.Pool
var pool *redis.Pool

func main() {
	//Staging
	absPath, _ := filepath.Abs("./app/stagingnode.crt")
	config, err := pgxpool.ParseConfig("postgres://reeturaj:reetu@35.184.195.70:26257/" +
		"isu_db?sslmode=verify-full&sslrootcert=" + absPath)
	//Production
	// config, err := pgxpool.ParseConfig("postgres://iserveutech:6yYnF!xj3uZG9kXk@35.202.138.160:26257/" +
	// 	"defaultdb?sslmode=require")
	if err != nil {
		fmt.Println("error configuring the database: ", err)
		log.Fatal("error configuring the database: ", err)
	}
	conn, err = pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		fmt.Println("error connecting to the database: ", err)
		log.Fatal("error connecting to the database: ", err)
	}

	defer conn.Close()
	pool = PoolInitRedis()
	//defer pool.Close()

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	fmt.Println("GRPC server started.")
	sp := keepalive.ServerParameters{
		Time:    time.Duration(3 * time.Hour), //10 sec
		Timeout: time.Duration(1 * time.Minute),
	}
	opts := []grpc.ServerOption{grpc.KeepaliveParams(sp), grpc.MaxConcurrentStreams(500), grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{MinTime: time.Duration(10 * time.Second), PermitWithoutStream: true})}
	grpcServer := grpc.NewServer(opts...)
	//For Credit,Debit,Refund,Status enquiry,Balance Check
	protobufpb.RegisterWalletServiceServer(grpcServer, &walletoperation.Server{Conn: conn})
	//For internal service
	protobuftest.RegisterServiceWalletServer(grpcServer, &walletoperation.Server{Conn: conn})
	//Register reflection service on gRPC server
	reflection.Register(grpcServer)
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %s", err)
	}

}
func PoolInitRedis() *redis.Pool {
	return &redis.Pool{
		MaxIdle:   1000,
		MaxActive: 1400,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", "35.244.45.55:6379")
			if err != nil {
				fmt.Println("dial err :", err)
				c.Close()
				return nil, err
			}
			return c, err
		},
	}
}
