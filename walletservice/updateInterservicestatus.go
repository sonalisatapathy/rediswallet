package walletservice

import (
	"context"
	"fmt"
	protobuftest "grpcwallet/src/protobuf/protobuftest"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func UpdateInterservicestatus(debitreq Walletperform, creditreq Walletperform, response *protobuftest.Interwalletres, request *protobuftest.Interwalleteq, conn *pgxpool.Conn) {

	if request.GetSeriveType() == 1 {
		fmt.Println("Txn iD", debitreq.ApiId, "for", debitreq.Type)
		fmt.Println("Txn iD", creditreq.ApiId, "for", creditreq.Type)

		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, debitreq.ApiId, debitreq.UpdatedDate)
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, creditreq.ApiId, creditreq.UpdatedDate)
		go PubsubData(debitreq)
		go PubsubData(creditreq)
	} else if request.GetSeriveType() == 0 {
		fmt.Println("Txn iD ret", debitreq.Id, "for", debitreq.Type)
		fmt.Println("Txn iD ret", creditreq.Id, "for", creditreq.Type)
		debitreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		creditreq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, debitreq.Id, debitreq.UpdatedDate)
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, creditreq.Id, creditreq.UpdatedDate)
		go PubsubData(debitreq)
		go PubsubData(creditreq)
	}

}
