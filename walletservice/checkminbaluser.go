package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func CheckMinBalanceStatusUser(ctx context.Context, conn *pgxpool.Conn,req Walletperform) (error) {
	//debit user

	var PreviousBalance,minimumBalance float64
	
	if err := conn.QueryRow(ctx,
		"SELECT balance,minimum_balance FROM wallet WHERE id = $1",req.WalletId).Scan(&PreviousBalance, &minimumBalance); err != nil {

		fmt.Println("Error in selecting minimum_balance from user",err)
	}
	fmt.Println("minbal For User ", minimumBalance)
	if  PreviousBalance < req.Amount {
		return errors.New("7")//7
	}else if PreviousBalance-req.Amount < minimumBalance {
		return  errors.New("5")//5
	}
	
	return  nil
}