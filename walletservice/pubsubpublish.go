package walletservice

import (
	"context"
	"encoding/json"
	"fmt"

	"cloud.google.com/go/pubsub"
	"github.com/jinzhu/copier"
)

type message struct {
	Key string            `json:"key"`
	Msg PubsubRequestData `json:"message"`
}

var (
	topic *pubsub.Topic
	ctx   context.Context
)

func init() {
	fmt.Println("Init function start")
	ctx = context.Background()

	// err1 := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "app/iserveustaging-2a27f856e930.json")

	// if err1 != nil {

	// 	fmt.Println("unable to set", err1)

	// }

	// err1 := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "app/iserveustaging-4417e8881b5d.json")
	// // err1 := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "app/iserveustaging-2a27f856e930.json")
	// if err1 != nil {
	// 	// fmt.Println("unable to set Google application Credentials", err1)
	// 	log.Fatal("unable to set Google application Credentials", err1)
	// }

	fmt.Println("application set successfully")
	//Staging
	client, err := pubsub.NewClient(ctx, "iserveustaging")
	if err != nil {
		fmt.Println("Errror is:", err)
	}
	// err1 := os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", "app/creditapp-fe4e1d7b286b.json")
	// if err1 != nil {
	// 	// fmt.Println("unable to set Google application Credentials", err1)
	// 	log.Fatal("unable to set Google application Credentials", err1)
	// }
	//Production
	// client, err := pubsub.NewClient(ctx, "creditapp-29bf2")
	// if err != nil {
	// 	// fmt.Println("Errror is:", err)
	// 	log.Fatal("Errror is:", err)
	// }
	//Staging topic
	topicName := "staging_topic"
	//Production topic
	//	topicName := "iserveu_transaction_topic"

	topic = client.Topic(topicName)
	//defer topic.Stop()
}

func PubSubPublish(pubsubreq PubsubRequestData) {
	m := message{
		Key: "wallet1",
		Msg: pubsubreq,
	}
	fmt.Println("pubsub Req Data", m)
	jsonData, err := json.Marshal(m)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println("Data", string(jsonData))
	topic.PublishSettings.NumGoroutines = 1
	result := topic.Publish(ctx, &pubsub.Message{Data: jsonData})
	id, err := result.Get(ctx)
	if err != nil {
		fmt.Println("Error in publish", err, "of ID", pubsubreq.Txnid)
		if id == "" {
			//Republished in pubsub
			result := topic.Publish(ctx, &pubsub.Message{Data: jsonData})
			id, err = result.Get(ctx)
			if err != nil {
				fmt.Println("Again Error in publish", err, "of ID", pubsubreq.Txnid)
			}
			fmt.Println("Republished a message with msg ID:", id, "of ID", pubsubreq.Txnid)
			return
		} else {
			fmt.Println("Error in publish BUT id created", id, err, "of ID", pubsubreq.Txnid)
		}
	}
	fmt.Println("Published a message with msg ID:", id, "of ID", pubsubreq.Txnid)
}

func PubsubData(pubsubdata Walletperform) {
	if pubsubdata.Is_sl && pubsubdata.Id != 0 {
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &pubsubdata)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
		}
		pubsubreq.Id = fmt.Sprint(pubsubdata.Id)
		pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
		pubsubreq.WalletId = fmt.Sprint(pubsubdata.WalletId)
		pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ReversalId)
		PubSubPublish(pubsubreq)
	} else if pubsubdata.Is_sl && pubsubdata.ApiId != 0 {
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &pubsubdata)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
		}
		pubsubreq.Id = fmt.Sprint(pubsubdata.ApiId)
		pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
		pubsubreq.WalletId = fmt.Sprint(pubsubdata.ApiWalletId)
		pubsubreq.PreviousBalance = pubsubdata.ApiPreviousBalance
		pubsubreq.CurrentBalance = pubsubdata.ApiCurrentBalance
		pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ApiReversalId)
		PubSubPublish(pubsubreq)

	} else if !pubsubdata.Is_sl {
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &pubsubdata)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", pubsubdata.Id)
		}
		pubsubreq.Id = fmt.Sprint(pubsubdata.Id)
		pubsubreq.Txnid = fmt.Sprint(pubsubdata.TxnId)
		pubsubreq.WalletId = fmt.Sprint(pubsubdata.WalletId)
		pubsubreq.ReversalId = fmt.Sprint(pubsubdata.ReversalId)
		PubSubPublish(pubsubreq)
		pubsubapireq := PubsubRequestData{}
		apierr := copier.Copy(&pubsubapireq, &pubsubdata)
		if apierr != nil {
			fmt.Println("Error in Copy:: Insert request:", apierr.Error(), "of txn id:", pubsubdata.Id)
		}
		pubsubapireq.Id = fmt.Sprint(pubsubdata.ApiId)
		pubsubapireq.Txnid = fmt.Sprint(pubsubdata.TxnId)
		pubsubapireq.WalletId = fmt.Sprint(pubsubdata.ApiWalletId)
		pubsubapireq.PreviousBalance = pubsubdata.ApiPreviousBalance
		pubsubapireq.CurrentBalance = pubsubdata.ApiCurrentBalance
		pubsubapireq.ReversalId = fmt.Sprint(pubsubdata.ApiReversalId)
		PubSubPublish(pubsubapireq)
	}

}
