package walletservice

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func Checkwallet(conn *pgxpool.Pool, walletid int64) (float64, error) {
	var balance float64
	if err := conn.QueryRow(context.Background(), "select balance from wallet where id = $1 UNION select balance from wallet_api where id = $1", walletid).Scan(&balance); err != nil {
		return 0, err
	}
	fmt.Println("Balance", balance)
	return balance, nil
}
