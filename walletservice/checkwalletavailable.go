package walletservice

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

func Checkwalletavailable(ctx context.Context, conn *pgxpool.Conn, walletid1 int64, walletid2 int64) (int64, error) {

	var count int64
	if err := conn.QueryRow(context.Background(), "SELECT count(id) FROM wallet WHERE id in ($1,$2)", walletid1, walletid2).Scan(&count); err != nil {
		return 0, err
	}
	return count, nil
}
