package walletservice

import (
	"errors"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	protobufpbstatus "grpcwallet/src/protobuf/protobufpbstatus"
	"grpcwallet/src/protobuf/protobuftest"
)

func Checkvalidationforwallet(request *protobufpb.Request) (*protobufpb.Response, error) {
	response := &protobufpb.Response{}
	response.Txnid = request.Txnid
	response.Walletstatus = "FAILED"
	response.Wallettransactionid = 0
	response.Apiwallettransactionid = 0
	response.Walletstatuscode = 8
	if request.IsSl && ((request.Apiwalletid != 0 && request.Walletid != 0) || (request.Apiwalletid == 0 && request.Walletid == 0)) {
		response.Walletstatusdesc = "In single layer,One wallet ID is mandatory."
		return response, errors.New("In single layer,One wallet ID is mandatory.")
	} else if !request.IsSl && (request.Walletid == 0 || request.Apiwalletid == 0) {
		response.Walletstatusdesc = "In Double layer,Both wallet ID is mandatory."
		return response, errors.New("In Double layer,Both wallet ID is mandatory.")
	} else if request.Amount <= 0 {
		response.Walletstatusdesc = "Amount should be greater than 0."
		return response, errors.New("Amount should be greater than 0.")
	} else if request.Txnid <= 0 {
		response.Walletstatusdesc = "Txn ID should not be null or zero or -ve"
		return response, errors.New("Txn ID should not be null or zero or -ve")
	} else if request.TransactionType == "" {
		response.Walletstatusdesc = "Transaction type should not be NULL"
		return response, errors.New("transaction type should not be NULL")
	}
	return response, nil
}
func Checkvalidationforinterwalletservice(request *protobuftest.Interwalleteq) (*protobuftest.Interwalletres, error) {
	Interwalletresponse := &protobuftest.Interwalletres{}
	Interwalletresponse.Txnid = request.Tnxid
	Interwalletresponse.Walletstatus = "Failed"
	Interwalletresponse.Debittransactionid = 0
	Interwalletresponse.Credittransactionid = 0
	Interwalletresponse.Walletstatuscode = 6

	if request.DebitWallet == 0 {
		Interwalletresponse.Walletstatusdesc = "DebitWallet is mandatory"
		return Interwalletresponse, errors.New("DebitWallet is mandatory")
	} else if request.CreditWallet == 0 {
		Interwalletresponse.Walletstatusdesc = "CreditWallet is mandatory"
		return Interwalletresponse, errors.New("CreditWallet is mandatory")
	} else if request.Amount <= 0 {
		Interwalletresponse.Walletstatusdesc = "Amount should be greater than 0."
		return Interwalletresponse, errors.New("amount should be greater than 0")
	} else if request.Tnxid == 0 {
		Interwalletresponse.Walletstatusdesc = "Transaction ID should not be 0."
		return Interwalletresponse, errors.New("Transaction ID should not be 0.")
	} else if request.Tnxid < 0 {
		Interwalletresponse.Walletstatusdesc = "Transaction ID should not be Negative."
		return Interwalletresponse, errors.New("Transaction ID should not be Negative.")
	} else if request.SeriveType > 1 || request.SeriveType < 0 {
		Interwalletresponse.Walletstatusdesc = "Service type should  be 0 or 1."
		return Interwalletresponse, errors.New("Service type should  be 0 or 1.")
	} else if request.TransactionType == "" {
		Interwalletresponse.Walletstatusdesc = "Transaction type should not be NULL"
		return Interwalletresponse, errors.New("Transaction type should not be NULL")
	}
	return Interwalletresponse, nil
}
func Checkvalidationforstatusquery(request *protobufpb.StatusQueryRequest) (*protobufpb.StatusQueryResponse, error) {
	response := &protobufpb.StatusQueryResponse{}
	response.Txnid = request.Txnid
	response.Status = "FAILED"
	response.StatusCode = -1

	if request.Txnid == 0 {
		response.WalletStatusDesc = "Txn ID should not be null or zero"
		return response, errors.New("txn ID should not be null or zero")
	}
	return response, nil
}
func Checkvalidationforrefundamount(request *protobufpb.Refundrequest) (*protobufpb.Refundresponse, error) {

	response := &protobufpb.Refundresponse{}
	response.Txnid = request.Txnid
	response.Walletstatus = "FAILED"
	response.Wallettransactionid = 0
	response.Apiwallettransactionid = 0
	response.Walletstatuscode = 6

	if request.Txnid == 0 {
		response.Walletstatusdesc = "Txn ID should not be null or zero"
		return response, errors.New("txn ID should not be null or zero")
	} else if request.TransactionType == "" {
		response.Walletstatusdesc = "Transaction type should not be NULL"
		return response, errors.New("transaction type should not be NULL")
	} else if request.Ispartialrefund {
		if request.RefundStatusCode != 2 && request.RefundStatusCode != 3 {
			response.Walletstatusdesc = "Refund Not Allowed due to invalid statuscode"
			return response, errors.New("refund Not Allowed due to invalid statuscode")
		}
	} else if !request.Ispartialrefund {
		if request.RefundStatusCode != 4 && request.RefundStatusCode != 5 {
			response.Walletstatusdesc = "Refund Not Allowed due to invalid statuscode"
			return response, errors.New("refund Not Allowed due to invalid statuscode")
		}
	}
	return response, nil
}

func Checkvalidationforcommissionstatusquery(request *protobufpbstatus.StatusqueryRequest) (*protobufpbstatus.StatusqueryResponse, error) {
	response := &protobufpbstatus.StatusqueryResponse{}
	response.Txnid = request.Txnid
	response.Walletid = request.Walletid
	response.Status = "FAILED"
	response.StatusCode = -1

	if request.Txnid == 0 {
		response.WalletStatusDesc = "Txn ID should not be null or zero"
		return response, errors.New("txn ID should not be null or zero")
	} else if request.Walletid == 0 {
		response.WalletStatusDesc = "Walletid should not be null or zero"
		return response, errors.New("walletid should not be null or zero")
	}
	return response, nil
}
