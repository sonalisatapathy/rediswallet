package walletservice

import (
	"context"
	"fmt"
	protobufpb "grpcwallet/src/protobuf/protobufpb"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jinzhu/copier"
)

func UpdateRefund(updatereq Walletperform, response *protobufpb.Refundresponse, conn *pgxpool.Conn) {
	fmt.Println("Txn iD", updatereq.Id, "for", updatereq.Type)

	if updatereq.Is_sl && response.Apiwallettransactionid != 0 {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, response.Apiwallettransactionid, updatereq.UpdatedDate)
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &updatereq)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", updatereq.Id)
		}
		go PubSubPublish(pubsubreq)
	} else if updatereq.Is_sl && response.Wallettransactionid != 0 {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, updatereq.Id, updatereq.UpdatedDate)
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &updatereq)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", updatereq.Id)
		}
		go PubSubPublish(pubsubreq)
	} else {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, updatereq.Id, updatereq.UpdatedDate)
		pubsubreq := PubsubRequestData{}
		err := copier.Copy(&pubsubreq, &updatereq)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err.Error(), "of txn id:", updatereq.Id)
		}
		go PubSubPublish(pubsubreq)
		//wallet_api_ledger
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", response.Walletstatuscode, response.Walletstatus, updatereq.ApiId, updatereq.UpdatedDate)
		pubsubreq1 := PubsubRequestData{}
		err1 := copier.Copy(&pubsubreq, &updatereq)
		if err != nil {
			fmt.Println("Error in Copy:: Insert request:", err1.Error(), "of txn id:", updatereq.Id)
		}
		go PubSubPublish(pubsubreq1)
	}

}
