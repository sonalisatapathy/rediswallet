package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func CheckMinBalanceStatusApi(ctx context.Context, conn *pgxpool.Conn,req Walletperform) (error) {
	//debit user

	var ApiPreviousBalance,ApiminimumBalance float64
	
	if err := conn.QueryRow(ctx,
		"SELECT balance,minimum_balance FROM wallet_api WHERE id = $1",req. ApiWalletId ).Scan(&ApiPreviousBalance, &ApiminimumBalance); err != nil {

		fmt.Println("Error in selecting minimum_balance from user",err)
	}
	fmt.Println("minbal for Api ", ApiminimumBalance)
	if ApiPreviousBalance < req.Amount {
		return errors.New("8") //balance not available
	}else if ApiPreviousBalance-req.Amount < ApiminimumBalance {
		return  errors.New("6") //min bal not avail
	}

	return  nil
}