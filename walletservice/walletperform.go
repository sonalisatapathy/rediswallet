package walletservice

type Walletperform struct {
	Id                 int64
	ApiId              int64
	PreviousBalance    float64
	CurrentBalance     float64
	ApiPreviousBalance float64
	ApiCurrentBalance  float64
	WalletId           int64
	ApiWalletId        int64
	Amount             float64
	Is_sl              bool
	Type               int
	ReversalId         int64
	ApiReversalId      int64
	TxnId              int64
	Status             string
	StatusCode         int32
	CreatedDate        string
	UpdatedDate        string
	TransactionType    string
	MaximumBalance     float64
	MinimumBalance     float64
	ApimaximumBalnce   float64
	ApiminimumBalance  float64
	RefundStatusCode   int64
	Ispartial          bool
}

// type Reqresp struct {
// 	Gateway_transaction_id int64  `json:"gatewayTransactionId"`
// 	Txnid                  int64  `json:"txnid"`
// 	ParentTxn_id           int64  `json:"parentTxn_id"`
// 	Transaction_type       string `json:"transactionType"`
// 	User_id                int64  `json:"userId"`
// 	Walletstatus           string `json:"walletstatus"`
// 	Walletstatuscode       int    `json:"walletstatuscode"`
// 	Wallettransactionid    int64  `json:"wallettransactionid"`
// 	Walletstatusdesc       string `json:"walletstatusdesc"`
// 	WalletId               int64
// 	ApiWalletId            int64
// 	Amount                 float64
// 	Is_sl                  bool
// 	Type                   int
// 	Url                    string
// }
