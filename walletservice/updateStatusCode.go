package walletservice

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func UpdateStatusCode(updatereq Walletperform, conn *pgxpool.Conn) {

	if updatereq.Is_sl && updatereq.ApiId != 0 {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", updatereq.StatusCode, updatereq.Status, updatereq.ApiId, updatereq.UpdatedDate)

		go PubsubData(updatereq)
	} else if updatereq.Is_sl && updatereq.Id != 0 {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", updatereq.StatusCode, updatereq.Status, updatereq.Id, updatereq.UpdatedDate)
		go PubsubData(updatereq)
	} else {
		updatereq.UpdatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		conn.Exec(context.Background(), "UPDATE wallet_api_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", updatereq.StatusCode, updatereq.Status, updatereq.ApiId, updatereq.UpdatedDate)
		conn.Exec(context.Background(), "UPDATE wallet_ledger SET status_code = $1,status = $2,updateddate = $4 WHERE id =$3", updatereq.StatusCode, updatereq.Status, updatereq.Id, updatereq.UpdatedDate)
		go PubsubData(updatereq)
	}
}
