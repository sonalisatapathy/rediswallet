package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func CheckMaxBalanceStatusUser(ctx context.Context, conn *pgxpool.Conn,req Walletperform) (error) {
	//debit user

	var PreviousBalance,maximumBalance float64
	
	if err := conn.QueryRow(ctx,
		"SELECT balance,maximum_balance FROM wallet WHERE id = $1",req. WalletId ).Scan(&PreviousBalance, &maximumBalance); err != nil {

		fmt.Println("Error in selecting maximum_balance from user",err)
	}
	fmt.Println("maxbal for Api ", maximumBalance)
	 if PreviousBalance+req.Amount >= maximumBalance {
		return  errors.New("MaximumBalance Limit Exceed for the User")
	}

	return  nil
}