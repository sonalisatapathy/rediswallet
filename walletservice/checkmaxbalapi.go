package walletservice

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func CheckMaxBalanceStatusApi(ctx context.Context, conn *pgxpool.Conn,req Walletperform) (error) {
	//debit user

	var ApiPreviousBalance,ApimaximumBalance float64
	
	if err := conn.QueryRow(ctx,
		"SELECT balance,maximum_balance FROM wallet_api WHERE id = $1",req.ApiWalletId ).Scan(&ApiPreviousBalance, &ApimaximumBalance); err != nil {

		fmt.Println("Error in selecting maximum_balance from user",err)
	}
	fmt.Println("maxbal for Api ", ApimaximumBalance)
	 if ApiPreviousBalance+req.Amount >= ApimaximumBalance {
		return  errors.New("MaximumBalance Limit Exceed for the ApiUser")
	}

	return  nil
}