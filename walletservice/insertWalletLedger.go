package walletservice

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

func InsertWalletledger(ctx context.Context, conn *pgxpool.Conn, insertreq Walletperform) (Walletperform, error) {
	fmt.Println("Inside Insert Wallet:", insertreq.TxnId, "Wallet ID:", insertreq.WalletId, "Api Wallet ID:", insertreq.ApiWalletId)
	fmt.Println("Inside Insert Wallet:", insertreq.TxnId, "issl:", insertreq.Is_sl)
	var walletStatus, apiWalletStatus int8
	if err := conn.QueryRow(context.Background(), "SELECT status FROM wallet_api WHERE id=$1", insertreq.ApiWalletId).Scan(&walletStatus); err != nil && insertreq.ApiWalletId != 0 {
		fmt.Println("select error", err)
		return insertreq, errors.New("1")
	}
	if err := conn.QueryRow(context.Background(), "SELECT status FROM wallet WHERE id=$1 ", insertreq.WalletId).Scan(&apiWalletStatus); err != nil && insertreq.WalletId != 0 {
		fmt.Println("select error", err)
		return insertreq, errors.New("2")
	}
	fmt.Println("Inside Insert Wallet::WalletID:", insertreq.WalletId, "Walletstatus:", walletStatus)
	fmt.Println("Inside Insert Wallet::API WalletID:", insertreq.ApiWalletId, "Apiwalletstatus:", apiWalletStatus)
	if insertreq.Is_sl && (walletStatus != 1 && apiWalletStatus == 0) {
		return insertreq, errors.New("3")
	} else if insertreq.Is_sl && (apiWalletStatus != 1 && walletStatus == 0) {
		return insertreq, errors.New("4")
	} else if !insertreq.Is_sl && (walletStatus != 1 || apiWalletStatus != 1) {
		return insertreq, errors.New("5")
	}

	if insertreq.Is_sl && insertreq.ApiWalletId != 0 {
		//For Single Layer API user INSERT in wallet api ledger
		insertreq.CreatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_api_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,$1,0,$2,'INITIATED',$3,$4,$5,0,$6,$7,$8) RETURNING id", insertreq.Amount, insertreq.ApiWalletId, insertreq.Type, insertreq.TxnId, insertreq.ApiReversalId, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.ApiId)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insertreq.Status = "INITIATED"
		insertreq.UpdatedDate = insertreq.CreatedDate
		go PubsubData(insertreq)
		return insertreq, nil
	} else if insertreq.Is_sl && insertreq.WalletId != 0 {
		//For Single Layer User INSERT in wallet ledger
		insertreq.CreatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,$1,0,$2,'INITIATED',$3,$4,$5,0,$6,$7,$8) RETURNING id", insertreq.Amount, insertreq.WalletId, insertreq.Type, insertreq.TxnId, insertreq.ReversalId, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.Id)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insertreq.Status = "INITIATED"
		insertreq.UpdatedDate = insertreq.CreatedDate
		go PubsubData(insertreq)
		return insertreq, nil
	} else if !insertreq.Is_sl && insertreq.WalletId != 0 && insertreq.ApiWalletId != 0 {
		insertreq.CreatedDate = time.Now().Format("2006-01-02 15:04:05.000000")
		err := conn.QueryRow(context.Background(),
			"INSERT INTO wallet_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,$1,0,$2,'INITIATED',$3,$4,$5,0,$6,$7,$8) RETURNING id", insertreq.Amount, insertreq.WalletId, insertreq.Type, insertreq.TxnId, insertreq.ReversalId, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.Id)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		err = conn.QueryRow(context.Background(),
			"INSERT INTO wallet_api_ledger(previousbalance,amount,currentbalance,walletid,status,type,txnid,reversalid,status_code,createddate,updateddate,transaction_type) VALUES (0,$1,0,$2,'INITIATED',$3,$4,$5,0,$6,$7,$8) RETURNING id", insertreq.Amount, insertreq.ApiWalletId, insertreq.Type, insertreq.TxnId, insertreq.ApiReversalId, insertreq.CreatedDate, insertreq.CreatedDate, insertreq.TransactionType).Scan(&insertreq.ApiId)
		if err != nil {
			fmt.Println("Inside Insert Wallet::Transaction ID", insertreq.TxnId, "Error for insert ledger", err)
			return insertreq, err
		}
		insertreq.Status = "INITIATED"
		insertreq.UpdatedDate = insertreq.CreatedDate
		go PubsubData(insertreq)
		return insertreq, nil
	} else {
		fmt.Println("Wrong data send", insertreq.TxnId)
		return insertreq, errors.New("Wrong data send")
	}
}
